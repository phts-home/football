// Football-Server-Control.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <winsvc.h>


// Service name
LPTSTR FootballServiceName = "Football-Server";


/*
 * Prints help information
 */
void printHelp()
{
	printf("Usage: Football-Server-Control.exe [-options...]\n\n");
	printf("where options include:\n\t-install - to install the service Football-Server with the specified program path for Football-Server.exe application following after this option, ex.: Football-Server-Control.exe -install \"D:\\Football\\Football-Server.exe\"\n\t-start - to start the service\n\t-stop - to stop the service\n\t-remove - to remove the service from OS\n");
}

/*
 * Processes program exit
 */
int processExit(int ret)
{
	printHelp();
	printf("\nPress any key to exit ");
	_getch();
	return ret;
}

/*
 * Processes program error
 */
int processError(char *text, DWORD errNo)
{
	printf("\n");
	printf("%s", text);
	printf("\nError code: %ld\n", errNo);
	printf("\nPress any key to continue (or exit) ");
	_getch();
	printf("\n");
	return errNo;
}



/*
 * Installs service FootballServiceName
 */
int install(LPCSTR path)
{
	printf("Installing... ");
	// open SCM
	SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
	if(!hSCManager) {
		return processError("Failed to open Service Control Manager", GetLastError());
	}
	// create service FootballServiceName
	SC_HANDLE hService = CreateService(hSCManager, FootballServiceName,
			FootballServiceName, SERVICE_ALL_ACCESS,
			SERVICE_WIN32_OWN_PROCESS, SERVICE_DEMAND_START,
			SERVICE_ERROR_NORMAL,
			path,
			NULL, NULL, NULL, NULL, NULL);
	if(!hService) {
		DWORD n = GetLastError();
		CloseServiceHandle(hSCManager);
		return processError("Failed to create service", n);
	}

	// close service handle
	CloseServiceHandle(hService);
	// close SCM's handle
	CloseServiceHandle(hSCManager);

	printf("Done\n");
	return 0;
}

/*
 * Starts the service
 */
int start()
{
	printf("Starting... ");
	SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(!hSCManager) {
		return processError("Failed to open Service Control Manager", GetLastError());
	}

	SC_HANDLE hService = OpenService(hSCManager, FootballServiceName, SERVICE_START);
	if(!hService) {
		return processError("Failed to open service", GetLastError());
	}

	if(!StartService(hService, 0, NULL)) {
		DWORD n = GetLastError();
		CloseServiceHandle(hService);
		CloseServiceHandle(hSCManager);
		return processError("Failed to start service", GetLastError());
	}

	CloseServiceHandle(hService);
	CloseServiceHandle(hSCManager);

	printf("Done\n");
	return 0;
}

/*
 * Stops the service
 */
int stop()
{
	printf("Stopping... ");
	SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(!hSCManager) {
		return processError("Failed to open Service Control Manager", GetLastError());
	}

	SC_HANDLE hService = OpenService(hSCManager, FootballServiceName, SERVICE_ALL_ACCESS);
	if(!hService) {
		return processError("Failed to open service", GetLastError());
	}

	// stop service
	SERVICE_STATUS ss;
	if(!ControlService(hService, SERVICE_CONTROL_STOP, &ss)) {
		DWORD n = GetLastError();
		CloseServiceHandle(hService);
		CloseServiceHandle(hSCManager);
		return processError("Failed to stop service", n);
	}

	CloseServiceHandle(hService);
	CloseServiceHandle(hSCManager);

	printf("Done\n");
	return 0;
}

/*
 * Removes service from OS
 */
int remove()
{
	printf("Removing... ");
	SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(!hSCManager) {
		return processError("Failed to open Service Control Manager", GetLastError());
	}

	SC_HANDLE hService = OpenService(hSCManager, FootballServiceName, SERVICE_STOP | DELETE);
	if(!hService) {
		return processError("Failed to open service", GetLastError());
	}
	
	// stop service
	SERVICE_STATUS ss;
	ControlService(hService, SERVICE_CONTROL_STOP, &ss);

	// remove service
	DeleteService(hService);

	CloseServiceHandle(hService);
	CloseServiceHandle(hSCManager);

	printf("Done\n");
	return 0;
}



/*
 * Program entry point
 */
int main(int argc, char* argv[])
{
	printf("Football-Server-Control\n");
	printf("(c) Phil Tsarik, 2010\n\n");

	if (argc == 1) {
		return processExit(-1);
	}
	int n = 1;
	while (n < argc) {
		if (strcmp(argv[n], "-install") == 0) {
			n++;
			if (n < argc) {
				install(argv[n]);
				n++;
			}
			printf("\n");
			continue;
		}
		if (strcmp(argv[n], "-start") == 0) {
			start();
			n++;
			printf("\n");
			continue;
		}
		if (strcmp(argv[n], "-stop") == 0) {
			stop();
			n++;
			printf("\n");
			continue;
		}
		if (strcmp(argv[n], "-remove") == 0) {
			remove();
			n++;
			printf("\n");
			continue;
		}
		return processExit(-1);
	}
	return 0;
}

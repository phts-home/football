//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Football-Client.rc
//
#define IDD_FOOTBALLCLIENT_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDB_BALL                        132
#define IDB_FIELD                       133
#define IDB_FIRSTPLAYER_RIGHT           135
#define IDB_SECONDPLAYER_LEFT           136
#define IDB_FIRSTGOALKEEPER             137
#define IDB_SECONDGOALKEEPER            138
#define IDR_MENU1                       139
#define IDB_FIRSTPLAYER_LEFT            140
#define IDB_SECONDPLAYER_RIGHT          141
#define IDB_USERSIGN                    142
#define IDD_CONNECT_DIALOG              143
#define IDD_ABOUT_DIALOG                144
#define IDB_POPUP                       145
#define IDB_BITMAP1                     146
#define IDB_POPUP2                      146
#define IDC_EDIT1                       1000
#define IDC_STATIC1                     1004
#define IDC_STATIC3                     1005
#define IDC_STATIC4                     1006
#define IDC_LIST1                       1007
#define IDC_BTN_SEARCH                  1008
#define IDC_RADIO1                      1009
#define IDC_RADIO2                      1010
#define IDC_RADIO3                      1011
#define ID_FILE_EXIT                    32771
#define ID_HELP_ABOUT                   32772
#define ID_GAME_CONNECTTOSERVER         32773
#define ID_GAME_CLOSECONNECTION         32774
#define ID_GAME_NEWGAME                 32775
#define ID_HELP_CONTROLS                32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        147
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

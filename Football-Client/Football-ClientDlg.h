// Football-ClientDlg.h : header file
//

#pragma once

#include "Football-ConnectDlg.h"
#include "Football-AboutDlg.h"
#include "FieldPainter.h"
#include "BallPainter.h"
#include "FootballerPainter.h"
#include "GoalkeeperPainter.h"
#include "ScorePainter.h"
#include "PopupPainter.h"


#define HOLD_TIME 5


// CFootballClientDlg dialog
class CFootballClientDlg : public CDialog
{
public:
	CFootballClientDlg(CWnd* pParent = NULL);

	enum { IDD = IDD_FOOTBALLCLIENT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

protected:
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnGameConnecttoserver();
	afx_msg void OnGameCloseconnection();
	afx_msg void OnGameNewgame();
	afx_msg void OnFileExit();
	afx_msg void OnClose();
	afx_msg void OnHelpControls();
	afx_msg void OnHelpAbout();


public:
	FieldPainter *field;
	FootballerPainter **firstTeamPlayers;
	FootballerPainter **secondTeamPlayers;
	GoalkeeperPainter *firstGoalkeeper;
	GoalkeeperPainter *secondGoalkeeper;
	BallPainter *ball;
	ScorePainter *score;

private:
	PopupPainter *popup;

public:
	void onGoal();
	void onServerFull();
	void onConnectionLost();

private:
	CConnectDlg connectDlg;
	CAboutDlg aboutDlg;
	bool leftPressed;
	bool rightPressed;
	bool upPressed;
	bool downPressed;
	int hold;
};

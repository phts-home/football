// Football-Client.h : main header file for the Football-Client application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"

#include "Parameters.h"


class CFootballClientApp : public CWinApp
{
public:
	CFootballClientApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CFootballClientApp theApp;
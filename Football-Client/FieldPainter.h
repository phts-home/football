#pragma once

#include "ObjectPainter.h"

class FieldPainter : public ObjectPainter
{
public:
	FieldPainter(HINSTANCE app);
	~FieldPainter();

public:
	void paint(HDC dc);

private:
	HBITMAP pic;
	int w;
	int h;
};

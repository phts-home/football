#pragma once

#include "ObjectPainter.h"

class TransparentObjectPainter abstract : public ObjectPainter
{
public:
	TransparentObjectPainter(HINSTANCE app);
	virtual ~TransparentObjectPainter();
	
protected:
	void paintTransparent(HDC dc, HBITMAP bitmap, int x, int y);
};

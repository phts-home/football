#include "StdAfx.h"
#include "PopupPainter.h"



PopupPainter::PopupPainter(HINSTANCE app) : ObjectPainter(app)
{
	enabled = false;
	x = 198;
	y = 222;
	n = 0;
	pic[0] = LoadBitmap(app, MAKEINTRESOURCE(IDB_POPUP));
	pic[1] = LoadBitmap(app, MAKEINTRESOURCE(IDB_POPUP2));
}

PopupPainter::~PopupPainter()
{
}

void PopupPainter::paint(HDC dc)
{
	if (enabled) {
		ObjectPainter::paint(dc, pic[n], x, y);
	}
}

void PopupPainter::setEnabled(bool enabled)
{
	n = 0;
	this->enabled = enabled;
}

void PopupPainter::blink()
{
	if (n == 1) {
		n = 0;
	} else {
		n = 1;
	}
}
#include "StdAfx.h"
#include "SearcherConnection.h"

#pragma comment(lib, "Ws2_32.lib")





//////////////////////////////////////////////////////////////////////////
// Private parameters
//

CConnectDlg * SearcherConnection::owner = NULL;

sockaddr_in SearcherConnection::dest;

SOCKET SearcherConnection::udpSocket = NULL;

HANDLE SearcherConnection::searcherThreadHandle = NULL;



//////////////////////////////////////////////////////////////////////////
// Public methods
//

int SearcherConnection::create()
{
	if (udpSocket != NULL) {
		return UDPSOCKET_ALREADYCREATED;
	}

	WSADATA wsd;
	if (WSAStartup(MAKEWORD(2,2), &wsd) != 0) {
		return UDPSOCKET_ERROR;
	}

	udpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (udpSocket == INVALID_SOCKET) {
		udpSocket = NULL;
		WSACleanup();
		return UDPSOCKET_ERROR;
	}

	dest.sin_family = AF_INET;
	dest.sin_port = htons(UDP_PORT);
	dest.sin_addr.s_addr = htonl(INADDR_BROADCAST);

	BOOL fBroadcast = TRUE;
	if (setsockopt(udpSocket, SOL_SOCKET, SO_BROADCAST, (char *)&fBroadcast, sizeof(BOOL)) == SOCKET_ERROR) {
		udpSocket = NULL;
		return UDPSOCKET_ERROR; 
	}

	return UDPSOCKET_OK;
}

int SearcherConnection::close()
{
	if (udpSocket == NULL) {
		return UDPSOCKET_OK;
	}

	if (searcherThreadHandle != NULL) {
		CloseHandle(searcherThreadHandle);
	}

	closesocket(udpSocket);
	WSACleanup();

	udpSocket = NULL;
	searcherThreadHandle = NULL;

	return UDPSOCKET_OK;
}

int SearcherConnection::searchForServers(CConnectDlg *owner)
{
	if (udpSocket == NULL) {
		return UDPSOCKET_ERROR;
	}
	SearcherConnection::owner = owner;

	searcherThreadHandle = CreateThread(NULL, 0, searchForServersThread, NULL, 0, NULL);

	return 0;
}



//////////////////////////////////////////////////////////////////////////
// Private methods
//

DWORD WINAPI SearcherConnection::searchForServersThread(PVOID pParam)
{
	const char* requestString = REQUEST_STRING;
	const char* responseString = RESPONSE_STRING;

	int r = sendto(udpSocket, requestString, (int)strlen(requestString), 0, (sockaddr *)&(dest), sizeof(dest));
	if (r == SOCKET_ERROR) {
		closesocket(udpSocket);
		return UDPSOCKET_ERROR;
	}

	char buf[15];

	struct sockaddr_in from;
	int fromSize = sizeof(from);

	while (TRUE) {
		r = recvfrom(udpSocket, buf, (int)strlen(responseString), 0, (struct sockaddr *) &from, &fromSize);
		if (r == SOCKET_ERROR) {
			return UDPSOCKET_ERROR;
		}
		buf[strlen(responseString)] = 0;
		if (strcmp(buf, responseString) == 0) {
			CString ip;
			ip.Format("%s", inet_ntoa(from.sin_addr));
			owner->addToList(ip);
		}
	}
	return 0;
}


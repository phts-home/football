// Football-AboutDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Football-Client.h"
#include "Football-AboutDlg.h"



IMPLEMENT_DYNAMIC(CAboutDlg, CDialog)

CAboutDlg::CAboutDlg(CWnd* pParent) : CDialog(CAboutDlg::IDD, pParent)
{

}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog()
{
	CStatic *static_ProgName = (CStatic *)GetDlgItem(IDC_STATIC1);
	CFont font;
	font.CreateFont(10, 0, 0, 0, FW_BOLD, TRUE, TRUE, TRUE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, "");
	static_ProgName->SetFont(&font);
	return TRUE;
}


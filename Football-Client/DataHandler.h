#pragma once

#include "Football-ClientDlg.h"
#include "ClientConnection.h"

class DataHandler
{
public:
	static void init(CFootballClientDlg *owner);
	static void reset();

	static void setBallEnabled(bool enabled);
	static void setBallPosition(int x, int y);

	static void setUserPlayer(int team, int no);

	static void setFirstTeamEnabled(bool enabled);
	static void setFirstGoalkeeperPosition(int x, int y);
	static void setFirstTeamPlayerEnabled(int no, bool enabled);
	static void setFirstTeamPlayerDirection(int no, int direction);
	static void setFirstTeamPlayerPosition(int no, int x, int y);

	static void setSecondTeamEnabled(bool enabled);
	static void setSecondGoalkeeperPosition(int x, int y);
	static void setSecondTeamPlayerEnabled(int no, bool enabled);
	static void setSecondTeamPlayerDirection(int no, int direction);
	static void setSecondTeamPlayerPosition(int no, int x, int y);

	static void setScore(int firstNum, int secondNum);

private:
	static CFootballClientDlg *owner;
};

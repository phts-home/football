#pragma once
#include "ObjectPainter.h"

class ScorePainter : public ObjectPainter
{
public:
	ScorePainter(HINSTANCE app, int firstNum, int secondNum);
	~ScorePainter();

public:
	void paint(HDC dc);
	void setScore(int firstNum, int secondNum);

private:
	int firstNum;
	int secondNum;
	char *scoreString;

private:
	void buildString();
};

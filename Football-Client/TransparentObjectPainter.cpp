#include "StdAfx.h"
#include "TransparentObjectPainter.h"

TransparentObjectPainter::TransparentObjectPainter(HINSTANCE app) : ObjectPainter(app)
{
}

TransparentObjectPainter::~TransparentObjectPainter()
{
}

void TransparentObjectPainter::paintTransparent(HDC dc, HBITMAP bitmap, int x, int y)
{
	BITMAP bt;
	GetObject(bitmap, sizeof(BITMAP), &bt);
	HDC dcMem2 = CreateCompatibleDC(dc);
	HBITMAP tempOld = (HBITMAP)SelectObject(dcMem2, bitmap);
	TransparentBlt(dc, x, y, bt.bmWidth, bt.bmHeight, dcMem2, 0, 0, bt.bmWidth, bt.bmHeight, RGB(255, 0, 255));
	SelectObject(dcMem2, tempOld);
	DeleteDC(dcMem2);
}
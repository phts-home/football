// Football-ConnectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Football-Client.h"
#include "Football-ConnectDlg.h"

#include "SearcherConnection.h"



IMPLEMENT_DYNAMIC(CConnectDlg, CDialog)

CConnectDlg::CConnectDlg(CWnd* pParent) : CDialog(CConnectDlg::IDD, pParent)
{
	serverHost = (char *)calloc(50, sizeof(char));
	team = 0;
}

CConnectDlg::~CConnectDlg()
{
	delete [] serverHost;
}

void CConnectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CConnectDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CConnectDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_SEARCH, &CConnectDlg::OnBnClickedBtnSearch)
	ON_WM_DESTROY()
	ON_LBN_DBLCLK(IDC_LIST1, &CConnectDlg::OnLbnDblclkList1)
	ON_LBN_SELCHANGE(IDC_LIST1, &CConnectDlg::OnLbnSelchangeList1)
	ON_BN_CLICKED(IDC_RADIO1, &CConnectDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CConnectDlg::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_RADIO3, &CConnectDlg::OnBnClickedRadio3)
END_MESSAGE_MAP()

BOOL CConnectDlg::OnInitDialog()
{
	CEdit *edit_ServerHost = (CEdit *)GetDlgItem(IDC_EDIT1);
	edit_ServerHost->SetWindowText((LPCTSTR)"");

	CButton *radio_TeamAuto = (CButton *)GetDlgItem(IDC_RADIO1);
	radio_TeamAuto->SetCheck(BST_CHECKED);
	
	return TRUE;
}

void CConnectDlg::OnDestroy()
{
	CDialog::OnDestroy();

	SearcherConnection::close();
}

void CConnectDlg::OnBnClickedOk()
{
	CEdit *edit_ServerHost = (CEdit *)GetDlgItem(IDC_EDIT1);
	edit_ServerHost->GetWindowText(serverHost, 50);
	if (strcmp(serverHost, "") == 0) {
		ProgramErrorMessageBox(MESSAGE_SERVERHOST_EMPTY);
		return;
	}
	OnOK();
}

void CConnectDlg::OnBnClickedBtnSearch()
{
	SearcherConnection::close();

	SearcherConnection::create();

	SearcherConnection::searchForServers(this);
}

void CConnectDlg::OnLbnDblclkList1()
{
	CListBox *list_servers = (CListBox *)GetDlgItem(IDC_LIST1);
	CString sel;
	list_servers->GetText(list_servers->GetCurSel(), sel);

	CEdit *edit_ServerHost = (CEdit *)GetDlgItem(IDC_EDIT1);
	edit_ServerHost->SetWindowText(sel);
}

void CConnectDlg::OnLbnSelchangeList1()
{
	CListBox *list_servers = (CListBox *)GetDlgItem(IDC_LIST1);
	CString sel;
	list_servers->GetText(list_servers->GetCurSel(), sel);

	CEdit *edit_ServerHost = (CEdit *)GetDlgItem(IDC_EDIT1);
	edit_ServerHost->SetWindowText(sel);
}

void CConnectDlg::OnBnClickedRadio1()
{
	team = 0;
}

void CConnectDlg::OnBnClickedRadio2()
{
	team = 1;
}

void CConnectDlg::OnBnClickedRadio3()
{
	team = 2;
}


void CConnectDlg::addToList(CString s)
{
	CListBox *list_servers = (CListBox *)GetDlgItem(IDC_LIST1);
	list_servers->AddString((LPCTSTR)s);
}

char * CConnectDlg::getServerHost()
{
	return serverHost;
}

int CConnectDlg::getTeam()
{
	return team;
	/*CButton *radio_TeamAuto = (CButton *)GetDlgItem(IDC_RADIO1);
	if (radio_TeamAuto->GetCheck() == BST_CHECKED) {
		return 0;
	}

	CButton *radio_TeamRed = (CButton *)GetDlgItem(IDC_RADIO2);
	if (radio_TeamAuto->GetCheck() == BST_CHECKED) {
		return 1;
	}

	CButton *radio_TeamBlue = (CButton *)GetDlgItem(IDC_RADIO3);
	if (radio_TeamAuto->GetCheck() == BST_CHECKED) {
		return 2;
	}*/
	return 0;
}


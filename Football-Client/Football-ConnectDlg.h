#pragma once

#include "Football-Client.h"
#include "afxwin.h"


// CConnectDlg dialog

class CConnectDlg : public CDialog
{
	DECLARE_DYNAMIC(CConnectDlg)

public:
	CConnectDlg(CWnd* pParent = NULL);
	~CConnectDlg();

	enum { IDD = IDD_CONNECT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBtnSearch();
	afx_msg void OnDestroy();
	afx_msg void OnLbnDblclkList1();
	afx_msg void OnLbnSelchangeList1();
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio3();

public:
	void addToList(CString s);
	char *getServerHost();
	int getTeam();

private:
	char *serverHost;
	int team;
};

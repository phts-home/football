#include "StdAfx.h"
#include "ClientConnection.h"

#include "ClientConnection.h"
#include "DataHandler.h"

#pragma comment(lib, "Ws2_32.lib")



//////////////////////////////////////////////////////////////////////////
// Private parameters
//

CFootballClientDlg * ClientConnection::owner = NULL;

SOCKET ClientConnection::clientSocket = NULL;

HANDLE ClientConnection::receiverThreadHandle = NULL;

bool ClientConnection::connected = false;




//////////////////////////////////////////////////////////////////////////
// Public methods
//

void ClientConnection::init(CFootballClientDlg *owner)
{
	ClientConnection::owner = owner;
}

int ClientConnection::create(const char *host, int team)
{
	if (clientSocket != NULL) {
		return CLIENTSOCKET_ALREADYCREATED;
	}
	connected = false;

	WSADATA wsd;

	if (WSAStartup(MAKEWORD(2,2), &wsd) != 0) {
		return CLIENTSOCKET_WSAFAILED;
	}

	struct sockaddr_in peer;

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (clientSocket == INVALID_SOCKET) {
		clientSocket = NULL;
		WSACleanup();
		return CLIENTSOCKET_SOCKETFAILED;
	}

	hostent* remoteHost = gethostbyname(host);
	if (WSAGetLastError() != 0) {
		if (WSAGetLastError() == 11001) {
			return CLIENTSOCKET_FAILED;
		} else {
			return CLIENTSOCKET_UNEXPECTEDERROR;
		}
	}
	peer.sin_addr.s_addr = inet_addr(inet_ntoa(*(struct in_addr *)remoteHost->h_addr_list[0]));
	peer.sin_family = AF_INET;
	peer.sin_port = htons(PORT);

	int rc;
	rc = connect(clientSocket, (struct sockaddr *)&peer, sizeof(peer));
	if (rc == SOCKET_ERROR) {
		clientSocket = NULL;
		WSACleanup();
		return CLIENTSOCKET_FAILED;
	}

	connected = true;

	sendRequestConnect(team);

	receiverThreadHandle = CreateThread(NULL, 0, receiveMessagesFromServerThread, NULL, 0, NULL);
	if (receiverThreadHandle == NULL) {
		clientSocket = NULL;
		WSACleanup();
		return CLIENTSOCKET_THREADFAILED;
	}

	DataHandler::setBallEnabled(true);

	return CLIENTSOCKET_OK;
}

int ClientConnection::requestClose()
{
	return sendAction(DATA_REQUESTCLOSE);
}

bool ClientConnection::isConnected()
{
	return connected;
}

int ClientConnection::sendAction(int action)
{
	if (!connected) {
		return -1;
	}
	return send(clientSocket, (char *)&action, sizeof(int), 0);
}



//////////////////////////////////////////////////////////////////////////
// Private methods
//

void ClientConnection::close()
{
	if (clientSocket == NULL) {
		return;
	}

	closesocket(clientSocket);

	while (receiverThreadHandle != NULL) {
		Sleep(25);
	}
}

void ClientConnection::doClose()
{
	clientSocket = NULL;
	connected = false;
	DataHandler::reset();
	WSACleanup();
}

int ClientConnection::sendRequestConnect(int team)
{
	sendAction(DATA_INIT);
	sendAction(team);
	return 0;
}

DWORD WINAPI ClientConnection::receiveMessagesFromServerThread(PVOID pParam)
{
	char buf[sizeof(int)];
	int received;
	while(TRUE) {
		received = recv(clientSocket, buf, sizeof(buf), 0);
		if (received <= 0) {
			ClientConnection::doClose();
			owner->onConnectionLost();
			return -1;
		}
		if (received != sizeof(buf)) {
			continue;
		}
		int* ptype = (int*)buf;
		int type = *ptype;
		switch (type) {
			case DATA_STATUS:
				{
					GameInformation *gameInformation = GameInformation::receiveStatus(clientSocket);

					if (gameInformation == NULL) {
						ClientConnection::doClose();
						return -3;
					}

					DataHandler::setScore(gameInformation->getFirstScoreNum(), gameInformation->getSecondScoreNum());

					DataHandler::setFirstTeamEnabled(gameInformation->isFirstTeamEnabled());
					DataHandler::setSecondTeamEnabled(gameInformation->isSecondTeamEnabled());

					for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
						if (gameInformation->isFirstTeamPlayerEnabled(i)) {
							DataHandler::setFirstTeamPlayerEnabled(i, true);
							DataHandler::setFirstTeamPlayerDirection(i, gameInformation->getFirstTeamPlayerDirection(i));
							DataHandler::setFirstTeamPlayerPosition(i, gameInformation->getFirstTeamPlayerPositionX(i), gameInformation->getFirstTeamPlayerPositionY(i));
							DataHandler::setFirstGoalkeeperPosition(gameInformation->getFirstGoalkeeperPositionX(), gameInformation->getFirstGoalkeeperPositionY());
						} else {
							DataHandler::setFirstTeamPlayerEnabled(i, false);
						}
						if (gameInformation->isSecondTeamPlayerEnabled(i)) {
							DataHandler::setSecondTeamPlayerEnabled(i, true);
							DataHandler::setSecondTeamPlayerDirection(i, gameInformation->getSecondTeamPlayerDirection(i));
							DataHandler::setSecondTeamPlayerPosition(i, gameInformation->getSecondTeamPlayerPositionX(i), gameInformation->getSecondTeamPlayerPositionY(i));
							DataHandler::setSecondGoalkeeperPosition(gameInformation->getSecondGoalkeeperPositionX(), gameInformation->getSecondGoalkeeperPositionY());
						} else {
							DataHandler::setSecondTeamPlayerEnabled(i, false);
						}
					}

					DataHandler::setBallPosition(gameInformation->getBallPositionX(), gameInformation->getBallPositionY());

					delete gameInformation;
					break;
				}
			case DATA_GOAL:
				{
					owner->onGoal();
					break;
				}
			case DATA_INIT:
				{
					char b[sizeof(int)];
					recv(clientSocket, b, sizeof(int), 0);
					int* pt = (int *)b;
					int t = *pt;
					recv(clientSocket, b, sizeof(int), 0);
					int* pn = (int *)b;
					int n = *pn;
					DataHandler::setUserPlayer(t, n);
					break;
				}
			case DATA_SERVERFULL:
				{
					closesocket(clientSocket);
					ClientConnection::doClose();
					owner->onServerFull();
					return 1;
				}
			case DATA_ACCEPTCLOSE:
				{
					closesocket(clientSocket);
					ClientConnection::doClose();
					return 1;
				}
		}
	}
	return 0;
}



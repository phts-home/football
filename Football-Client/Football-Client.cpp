// Football-Client.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Football-Client.h"
#include "Football-ClientDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFootballClientApp

BEGIN_MESSAGE_MAP(CFootballClientApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CFootballClientApp construction

CFootballClientApp::CFootballClientApp()
{
}


// The one and only CFootballClientApp object

CFootballClientApp theApp;


// CFootballClientApp initialization

BOOL CFootballClientApp::InitInstance()
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);


	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Create main window
	CFootballClientDlg dlg;
	m_pMainWnd = &dlg;
	
	dlg.DoModal();

	return FALSE;
}

#include "StdAfx.h"
#include "ScorePainter.h"

ScorePainter::ScorePainter(HINSTANCE app, int firstNum, int secondNum) : ObjectPainter(app)
{
	scoreString = (char *)calloc(10, sizeof(char));
	setScore(firstNum, secondNum);
}

ScorePainter::~ScorePainter()
{
	delete [] scoreString;
}

void ScorePainter::paint(HDC dc)
{
	ObjectPainter::paint(dc, scoreString, 389, 606);
}

void ScorePainter::setScore(int firstNum, int secondNum)
{
	this->firstNum = firstNum;
	this->secondNum = secondNum;
	buildString();
}

void ScorePainter::buildString()
{
	strcpy_s(scoreString, 10,"");
	char *b = (char *)calloc(10, sizeof(char));
	_itoa_s(firstNum, b, 10, 10);
	strcat_s(scoreString, 10, b);

	strcat_s(scoreString, 10, ":");

	_itoa_s(secondNum, b, 10, 10);
	strcat_s(scoreString, 10, b);
	delete [] b;
}
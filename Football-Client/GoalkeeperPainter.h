#pragma once

#include "TransparentObjectPainter.h"

class GoalkeeperPainter : public TransparentObjectPainter
{
public:
	GoalkeeperPainter(HINSTANCE app, int team);
	~GoalkeeperPainter();

public:
	void paint(HDC dc);
	void setX(int x);
	void setY(int y);
	void setEnabled(bool enabled);

private:
	HBITMAP pic;
	int x;
	int y;
	bool enabled;
};

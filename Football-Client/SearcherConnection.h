#pragma once

#include <Winsock2.h>

#include "Football-ConnectDlg.h"


#define UDP_PORT 4445

#define UDPSOCKET_ALREADYCREATED 1
#define UDPSOCKET_ERROR 2
#define UDPSOCKET_OK 0

#define REQUEST_STRING "Football-Cl"
#define RESPONSE_STRING "Football-S"


/*
 * Class makes requests and broadcasts them to find available servers in the network
 */
class SearcherConnection
{
public:
	//
	// Creates UPD-connection
	//
	static int create();

	//
	// Closes UDP-connection
	//
	static int close();

	//
	// Searches for servers in the network and adds them into the owner's list
	//
	static int searchForServers(CConnectDlg *owner);


private:
	static CConnectDlg *owner;
	static SOCKET udpSocket;
	static sockaddr_in dest;
	static HANDLE searcherThreadHandle;

private:
	static DWORD WINAPI searchForServersThread(PVOID pParam);

};

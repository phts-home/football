#include "StdAfx.h"
#include "DataHandler.h"



CFootballClientDlg *DataHandler::owner = NULL;




void DataHandler::init(CFootballClientDlg *_owner)
{
	owner = _owner;
}

void DataHandler::reset()
{
	owner->ball->setEnabled(false);
	owner->firstGoalkeeper->setEnabled(false);
	owner->secondGoalkeeper->setEnabled(false);
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		owner->firstTeamPlayers[i]->setEnabled(false);
		owner->secondTeamPlayers[i]->setEnabled(false);
		owner->firstTeamPlayers[i]->setUser(false);
		owner->secondTeamPlayers[i]->setUser(false);
	}
}



void DataHandler::setBallEnabled(bool enabled)
{
	owner->ball->setEnabled(enabled);
}

void DataHandler::setBallPosition(int x, int y)
{
	owner->ball->setX(x);
	owner->ball->setY(y);
}



void DataHandler::setUserPlayer(int team, int no)
{
	if (team == 0) {
		owner->firstTeamPlayers[no]->setUser(true);
	} else {
		owner->secondTeamPlayers[no]->setUser(true);
	}
}



void DataHandler::setFirstTeamEnabled(bool enabled)
{
	owner->firstGoalkeeper->setEnabled(enabled);
}

void DataHandler::setFirstGoalkeeperPosition(int x, int y)
{
	owner->firstGoalkeeper->setX(x);
	owner->firstGoalkeeper->setY(y);
}

void DataHandler::setFirstTeamPlayerEnabled(int no, bool enabled)
{
	owner->firstTeamPlayers[no]->setEnabled(enabled);
}

void DataHandler::setFirstTeamPlayerDirection(int no, int direction)
{
	owner->firstTeamPlayers[no]->setDirection(direction);
}

void DataHandler::setFirstTeamPlayerPosition(int no, int x, int y)
{
	owner->firstTeamPlayers[no]->setX(x);
	owner->firstTeamPlayers[no]->setY(y);
}


void DataHandler::setSecondTeamEnabled(bool enabled)
{
	owner->secondGoalkeeper->setEnabled(enabled);
}

void DataHandler::setSecondGoalkeeperPosition(int x, int y)
{
	owner->secondGoalkeeper->setX(x);
	owner->secondGoalkeeper->setY(y);
}

void DataHandler::setSecondTeamPlayerEnabled(int no, bool enabled)
{
	owner->secondTeamPlayers[no]->setEnabled(enabled);
}

void DataHandler::setSecondTeamPlayerDirection(int no, int direction)
{
	owner->secondTeamPlayers[no]->setDirection(direction);
}

void DataHandler::setSecondTeamPlayerPosition(int no, int x, int y)
{
	owner->secondTeamPlayers[no]->setX(x);
	owner->secondTeamPlayers[no]->setY(y);
}


void DataHandler::setScore(int firstNum, int secondNum)
{
	owner->score->setScore(firstNum, secondNum);
}



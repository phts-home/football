#pragma once

#include <stdlib.h>
#include <Winsock2.h>

#include "Football-ClientDlg.h"
#include "..\\Football-Server\\GameInformation.h"


#define PORT 4444

#define CLIENTSOCKET_UNEXPECTEDERROR -2
#define CLIENTSOCKET_ALREADYCREATED 1
#define CLIENTSOCKET_WSAFAILED 2
#define CLIENTSOCKET_WRONGHOST 3
#define CLIENTSOCKET_SOCKETFAILED 4
#define CLIENTSOCKET_FAILED 5
#define CLIENTSOCKET_THREADFAILED 6
#define CLIENTSOCKET_OK 0

#define ACTION_LEFT 1
#define ACTION_RIGHT 2
#define ACTION_UP 3
#define ACTION_DOWN 4
#define ACTION_UPLEFT 5
#define ACTION_UPRIGHT 6
#define ACTION_DOWNLEFT 7
#define ACTION_DOWNRIGHT 8
#define ACTION_KICK 9

#define DATA_INIT 100
#define DATA_STATUS 101
#define DATA_SERVERFULL 102
#define DATA_REQUESTCLOSE 103
#define DATA_ACCEPTCLOSE 104
#define DATA_NEWGAME 105
#define DATA_GOAL 106

#define AUTO_TEAM 0
#define RED_TEAM 1
#define BLUE_TEAM 2



/*
 * Describes client connection to the game server
 */
class ClientConnection
{
public:
	
	//
	// Initializes some parameters
	//
	static void init(CFootballClientDlg *owner);

	//
	// Tries to connect to the game server
	// Returns CONNECT_OK if connection is success
	//
	static int create(const char *host, int team);

	//
	// Sends to server a request to close
	//
	static int requestClose();

	//
	// Tests whether the client is connected
	// Returns true if client is connected or otherwise false
	//
	static bool isConnected();

	//
	// Sends user action to the server
	//
	static int sendAction(int action);

	

private:
	static CFootballClientDlg *owner;
	static SOCKET clientSocket;
	static HANDLE receiverThreadHandle;
	static bool connected;

private:

	static void close();

	//
	// Closes the connection
	//
	static void doClose();

	static int sendRequestConnect(int team);

	//
	// Start function for the message receiver thread from server
	//
	static DWORD WINAPI receiveMessagesFromServerThread(PVOID pParam);
};

#pragma once

#include "TransparentObjectPainter.h"



class BallPainter : public TransparentObjectPainter
{
public:
	BallPainter(HINSTANCE app);
	~BallPainter();

public:
	void paint(HDC dc);
	void setX(int x);
	void setY(int y);
	void setEnabled(bool enabled);

private:
	HBITMAP pic;
	int x;
	int y;
	bool enabled;
};

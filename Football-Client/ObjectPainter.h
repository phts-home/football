#pragma once

#include "Football-Client.h"

class ObjectPainter abstract
{
public:
	ObjectPainter(HINSTANCE app);
	virtual ~ObjectPainter();

public:
	virtual void paint(HDC dc) = 0;

protected:
	void paint(HDC dc, HBITMAP bitmap, int x, int y);
	void paint(HDC dc, char *text, int x, int y);

protected:
	HINSTANCE app;
};

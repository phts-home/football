// Football-ClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Football-Client.h"
#include "Football-ClientDlg.h"
#include <string.h>

#include "ClientConnection.h"
#include "DataHandler.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif




CFootballClientDlg::CFootballClientDlg(CWnd* pParent) : CDialog(CFootballClientDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	field = new FieldPainter(theApp.m_hInstance);
	firstTeamPlayers = (FootballerPainter **)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(FootballerPainter *));
	secondTeamPlayers = (FootballerPainter **)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(FootballerPainter *));
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		firstTeamPlayers[i] = new FootballerPainter(theApp.m_hInstance, 0);
		secondTeamPlayers[i] = new FootballerPainter(theApp.m_hInstance, 1);
	}
	firstGoalkeeper = new GoalkeeperPainter(theApp.m_hInstance, 0);
	secondGoalkeeper = new GoalkeeperPainter(theApp.m_hInstance, 1);
	ball = new BallPainter(theApp.m_hInstance);
	score = new ScorePainter(theApp.m_hInstance, 0, 0);
	popup = new PopupPainter(theApp.m_hInstance);

	leftPressed = false;
	rightPressed = false;
	upPressed = false;
	downPressed = false;

	hold = 0;
}

void CFootballClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFootballClientDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_COMMAND(ID_GAME_CONNECTTOSERVER, &CFootballClientDlg::OnGameConnecttoserver)
	ON_COMMAND(ID_GAME_CLOSECONNECTION, &CFootballClientDlg::OnGameCloseconnection)
	ON_COMMAND(ID_FILE_EXIT, &CFootballClientDlg::OnFileExit)
	ON_COMMAND(ID_HELP_ABOUT, &CFootballClientDlg::OnHelpAbout)
	ON_COMMAND(ID_GAME_NEWGAME, &CFootballClientDlg::OnGameNewgame)
	ON_COMMAND(ID_HELP_CONTROLS, &CFootballClientDlg::OnHelpControls)
END_MESSAGE_MAP()

BOOL CFootballClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	this->SetWindowPos(NULL, 400, 100, 810, 690, 0);

	ClientConnection::init(this);
	DataHandler::init(this);

	SetTimer(1, 25, NULL);//����������
	SetTimer(2, 50, NULL);//�������� ������ 

	return TRUE;
}

void CFootballClientDlg::OnDestroy()
{
	CDialog::OnDestroy();

	KillTimer(1);
	KillTimer(2);
	KillTimer(3);

	delete field;
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		delete firstTeamPlayers[i];
		delete secondTeamPlayers[i];
	}
	delete [] firstTeamPlayers;
	delete [] secondTeamPlayers;
	delete firstGoalkeeper;
	delete secondGoalkeeper;
	delete ball;
	delete score;
	delete popup;
}

BOOL CFootballClientDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN) {
		return FALSE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CFootballClientDlg::OnPaint()
{
	HDC dc = ::GetDC(m_hWnd);		//�������� �������� ���������� (������ ���� �������)
	RECT rc;
	GetClientRect(&rc);	//�������� ������ �������

	HBITMAP btOld;	//��������� ���������� �������� ��� �������� ��������� ����������
	HDC dcMem = CreateCompatibleDC(dc);	//������� ����� ��������� ���������� � ������
	HBITMAP btMem = CreateCompatibleBitmap(dc,rc.right,rc.bottom);	//������� ����������� �������� ��� ��������� � ������

	btOld = (HBITMAP)SelectObject(dcMem,btMem);//�������� ����������� �������� � ����� ��������� ���������� � ������

	field->paint(dcMem);
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		firstTeamPlayers[i]->paint(dcMem);
		secondTeamPlayers[i]->paint(dcMem);
	}
	firstGoalkeeper->paint(dcMem);
	secondGoalkeeper->paint(dcMem);
	ball->paint(dcMem);
	score->paint(dcMem);
	popup->paint(dcMem);

	::BitBlt(dc, 0, 0, rc.right, rc.bottom, dcMem, 0, 0, SRCCOPY);//�������� ���������� ��������� ���������� �� �������� ��������

	SelectObject(dcMem,btOld);//�������� ������ ���������� ����� ��������� ���������� � ������ �������
	DeleteObject(btMem);// ������� ����������� �������� ��� ���������
	DeleteDC(dcMem);// ������� ����� ���������

	::ReleaseDC(m_hWnd,dc);// ����������� �������� ����������
	CDialog::OnPaint();// �������� ������������ ������� ����������� ������
}

HCURSOR CFootballClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CFootballClientDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent) {
		case 1:
			{
				OnPaint();
				break;
			}
		case 2:
			{
				if (hold > 0) {
					break;
				}
				if (leftPressed) {
					if (upPressed) {
						ClientConnection::sendAction(ACTION_UPLEFT);
						break;
					}
					if (downPressed) {
						ClientConnection::sendAction(ACTION_DOWNLEFT);
						break;
					}
					ClientConnection::sendAction(ACTION_LEFT);
					break;
				}
				if (rightPressed) {
					if (upPressed) {
						ClientConnection::sendAction(ACTION_UPRIGHT);
						break;
					}
					if (downPressed) {
						ClientConnection::sendAction(ACTION_DOWNRIGHT);
						break;
					}
					ClientConnection::sendAction(ACTION_RIGHT);
					break;
				}
				if (upPressed) {
					ClientConnection::sendAction(ACTION_UP);
					break;
				}
				if (downPressed) {
					ClientConnection::sendAction(ACTION_DOWN);
					break;
				}
				break;
			}
		case 3:
			{
				popup->blink();
				hold--;
				if (hold == 0) {
					popup->setEnabled(false);
					KillTimer(3);
				}
				break;
			}
	}
	CDialog::OnTimer(nIDEvent);
}

void CFootballClientDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch (nChar) {
		case 37:	// left
		case 65:	// A
			{
				leftPressed = true;
				break;
			}
		case 38:	// up
		case 87:	// W
			{
				upPressed = true;
				break;
			}
		case 39:	// right
		case 68:	// D
			{
				rightPressed = true;
				break;
			}
		case 40:	// down
		case 83:	// S
			{
				downPressed = true;
				break;
			}
		case 17:	// ctrl
		case 13:	// enter
			{
				ClientConnection::sendAction(ACTION_KICK);
				break;
			}
		case 116:	// F5
			{
				OnGameConnecttoserver();
			}
	}
	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CFootballClientDlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch (nChar) {
		case 37:	// left
		case 65:	// A
			{
				leftPressed = false;
				break;
			}
		case 38:	// up
		case 87:	// W
			{
				upPressed = false;
				break;
			}
		case 39:	// right
		case 68:	// D
			{
				rightPressed = false;
				break;
			}
		case 40:	// down
		case 83:	// S
			{
				downPressed = false;
				break;
			}
	}
	CDialog::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CFootballClientDlg::OnClose()
{
	OnGameCloseconnection();

	CDialog::OnClose();
}

void CFootballClientDlg::OnFileExit()
{
	OnGameCloseconnection();

	OnOK();
}

void CFootballClientDlg::OnGameConnecttoserver()
{
	INT_PTR r = connectDlg.DoModal();
	if (r == IDOK) {
		char *s = connectDlg.getServerHost();
		int team = connectDlg.getTeam();
		switch (ClientConnection::create(s, team)) {
			case CLIENTSOCKET_UNEXPECTEDERROR:
				{
					ProgramErrorMessageBox(MESSAGE_CONNECT_UNEXPECTEDERROR);
					break;
				}
			case CLIENTSOCKET_ALREADYCREATED:
				{
					ProgramErrorMessageBox(MESSAGE_CONNECT_ALREADYCREATED);
					break;
				}
			case CLIENTSOCKET_WSAFAILED:
			case CLIENTSOCKET_WRONGHOST:
			case CLIENTSOCKET_SOCKETFAILED:
			case CLIENTSOCKET_THREADFAILED:
				{
					ProgramErrorMessageBox(MESSAGE_CONNECT_SOCKETFAILED);
					break;
				}
			case CLIENTSOCKET_FAILED:
				{
					ProgramErrorMessageBox(MESSAGE_CONNECT_FAILED);
					break;
				}
		}
	}
}

void CFootballClientDlg::OnGameCloseconnection()
{
	ClientConnection::requestClose();
	while (ClientConnection::isConnected()) {
		Sleep(25);
	}
}

void CFootballClientDlg::OnGameNewgame()
{
	ClientConnection::sendAction(DATA_NEWGAME);
}

void CFootballClientDlg::OnHelpAbout()
{
	aboutDlg.DoModal();
}

void CFootballClientDlg::OnHelpControls()
{
	ProgramInformationMessageBox(MESSAGE_CONTROLS);
}




void CFootballClientDlg::onGoal()
{
	hold = HOLD_TIME;
	SetTimer(3, 1000, NULL);
	
	popup->setEnabled(true);
}

void CFootballClientDlg::onServerFull()
{
	ProgramErrorMessageBox(MESSAGE_SERVERFULL);
}

void CFootballClientDlg::onConnectionLost()
{
	ProgramErrorMessageBox(MESSAGE_CONNECTONLOST);
}


#pragma once

#include "ObjectPainter.h"



class PopupPainter : public ObjectPainter
{
public:
	PopupPainter(HINSTANCE app);
	~PopupPainter();

public:
	void paint(HDC dc);
	void setEnabled(bool enabled);
	void blink();

private:
	HBITMAP pic[2];
	int x;
	int y;
	bool enabled;
	int n;
};

#include "StdAfx.h"
#include "BallPainter.h"



BallPainter::BallPainter(HINSTANCE app) : TransparentObjectPainter(app)
{
	enabled = false;
	x = 0;
	y = 0;
	pic = LoadBitmap(app, MAKEINTRESOURCE(IDB_BALL));
}

BallPainter::~BallPainter()
{
}

void BallPainter::paint(HDC dc)
{
	if (enabled) {
		paintTransparent(dc, pic, x-6, y-6);
	}
}

void BallPainter::setX(int x)
{
	this->x = x;
}

void BallPainter::setY(int y)
{
	this->y = y;
}

void BallPainter::setEnabled(bool enabled)
{
	this->enabled = enabled;
}
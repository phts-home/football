#include "StdAfx.h"
#include "FieldPainter.h"

FieldPainter::FieldPainter(HINSTANCE app) : ObjectPainter(app)
{
	w = 800;
	h = 646;
	pic = LoadBitmap(app, MAKEINTRESOURCE(IDB_FIELD));
}

FieldPainter::~FieldPainter()
{
}

void FieldPainter::paint(HDC dc)
{
	ObjectPainter::paint(dc, pic, 0, 0);
}


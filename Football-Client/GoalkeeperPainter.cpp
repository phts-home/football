#include "StdAfx.h"
#include "GoalkeeperPainter.h"



GoalkeeperPainter::GoalkeeperPainter(HINSTANCE app, int team) : TransparentObjectPainter(app)
{
	x = 0;
	y = 0;
	enabled = false;
	switch (team) {
		case 0:
			{
				pic = LoadBitmap(app, MAKEINTRESOURCE(IDB_FIRSTGOALKEEPER));
				break;
			}
		case 1:
			{
				pic = LoadBitmap(app, MAKEINTRESOURCE(IDB_SECONDGOALKEEPER));
				break;
			}
	}

}

GoalkeeperPainter::~GoalkeeperPainter()
{
}


void GoalkeeperPainter::paint(HDC dc)
{
	if (enabled) {
		paintTransparent(dc, pic, x-14, y-15);
	}
}

void GoalkeeperPainter::setX(int x)
{
	this->x = x;
}

void GoalkeeperPainter::setY(int y)
{
	this->y = y;
}

void GoalkeeperPainter::setEnabled(bool enabled)
{
	this->enabled = enabled;
}

#include "StdAfx.h"
#include "FootballerPainter.h"

FootballerPainter::FootballerPainter(HINSTANCE app, int team) : TransparentObjectPainter(app)
{
	x = 0;
	y = 0;
	enabled = false;
	direction = 0;
	user = false;
	switch (team) {
		case 0:
			{
				pic[0] = LoadBitmap(app, MAKEINTRESOURCE(IDB_FIRSTPLAYER_LEFT));
				pic[1] = LoadBitmap(app, MAKEINTRESOURCE(IDB_FIRSTPLAYER_RIGHT));
				break;
			}
		case 1:
			{
				pic[0] = LoadBitmap(app, MAKEINTRESOURCE(IDB_SECONDPLAYER_LEFT));
				pic[1] = LoadBitmap(app, MAKEINTRESOURCE(IDB_SECONDPLAYER_RIGHT));
				break;
			}
	}
	
}

FootballerPainter::~FootballerPainter()
{
}


void FootballerPainter::paint(HDC dc)
{
	if (enabled) {
		paintTransparent(dc, pic[direction], x-14, y-15);
		if (user) {
			paintTransparent(dc, pic[2], x-4, y-22);
		}
	}
}

void FootballerPainter::setX(int x)
{
	this->x = x;
}

void FootballerPainter::setY(int y)
{
	this->y = y;
}

void FootballerPainter::setEnabled(bool enabled)
{
	this->enabled = enabled;
}

void FootballerPainter::setDirection(int direction)
{
	this->direction = direction;
}

void FootballerPainter::setUser(bool user)
{
	this->user = user;
	if (user == true) {
		pic[2] = LoadBitmap(app, MAKEINTRESOURCE(IDB_USERSIGN));
	}
}

/*
 * Contains some parameters and constants
 */

#define PROGRAM_NAME "Football"

#define MESSAGE_SERVERHOST_EMPTY "Server host name must be not empty"

#define MESSAGE_CONNECT_UNEXPECTEDERROR "Unexpected error occurred"
#define MESSAGE_CONNECT_ALREADYCREATED "Connection already created"
#define MESSAGE_CONNECT_SOCKETFAILED "Unable to create connection"
#define MESSAGE_CONNECT_FAILED "Unable to connect to the server"
#define MESSAGE_CONNECT_ALREADYCREATED "Connection already created"

#define MESSAGE_SERVERFULL "Connection refused. Server is full"
#define MESSAGE_CONNECTONLOST "Connection is lost"

#define MESSAGE_CONTROLS "Move - arrows or A,W,S,D\n\nKick - ENTER or CTRL\n\nTake away the ball - ENTER or CTRL\n\nShow connection dialog - F5"

#define ProgramErrorMessageBox(text) MessageBox(text, PROGRAM_NAME, MB_OK | MB_ICONHAND)

#define ProgramInformationMessageBox(text) MessageBox(text, PROGRAM_NAME, MB_OK | MB_ICONASTERISK)
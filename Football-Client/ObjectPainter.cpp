#include "StdAfx.h"
#include "ObjectPainter.h"

ObjectPainter::ObjectPainter(HINSTANCE app)
{
	this->app = app;
}

ObjectPainter::~ObjectPainter()
{
}

void ObjectPainter::paint(HDC dc, HBITMAP bitmap, int x, int y)
{
	BITMAP bt;
	GetObject(bitmap, sizeof(BITMAP), &bt);
	::DrawState(dc, NULL, NULL, (LPARAM)bitmap, 0, x, y, bt.bmWidth, bt.bmHeight, DST_BITMAP);
}

void ObjectPainter::paint(HDC dc, char *text, int x, int y)
{
	::DrawState(dc, NULL, NULL, (LPARAM)text, 0, x, y, 0, 0, DST_TEXT);
}
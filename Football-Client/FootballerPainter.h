#pragma once

#include "TransparentObjectPainter.h"

class FootballerPainter : public TransparentObjectPainter
{
public:
	FootballerPainter(HINSTANCE app, int team);
	~FootballerPainter();

public:
	void paint(HDC dc);
	void setX(int x);
	void setY(int y);
	void setEnabled(bool enabled);
	void setDirection(int direction);
	void setUser(bool user);

private:
	HBITMAP pic[3];
	int x;
	int y;
	bool enabled;
	int direction;
	bool user;
};

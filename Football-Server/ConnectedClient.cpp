#include "StdAfx.h"
#include "ConnectedClient.h"


ConnectedClient::ConnectedClient(SOCKET socket, int id)
{
	this->socket = socket;
	this->id = id;
}

ConnectedClient::~ConnectedClient()
{
	close();
}



int ConnectedClient::close()
{
	return closesocket(socket);
}

SOCKET ConnectedClient::getSocket()
{
	return socket;
}

int ConnectedClient::getId()
{
	return id;
}

int ConnectedClient::getTeam() const
{
	return team;
}

void ConnectedClient::setTeam(int team)
{
	this->team = team;
}

int ConnectedClient::getNo() const
{
	return no;
}

void ConnectedClient::setNo( int no )
{
	this->no = no;
}

int ConnectedClient::recvFromClient(char *buf, int len)
{
	return recv(socket, buf, len, 0);
}


#pragma once

#include <Winsock2.h>

class ConnectedClient
{
public:
	ConnectedClient(SOCKET socket, int id);
	~ConnectedClient();

public:
	int close();
	SOCKET getSocket();
	int getId();
	int getTeam() const;
	void setTeam(int team);
	int getNo() const;
	void setNo(int no);
	int recvFromClient(char *buf, int len);

private:
	SOCKET socket;
	int id;
	int team;
	int no;
};

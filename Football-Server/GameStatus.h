#pragma once


#include "Position.h"
#include "Score.h"
#include "ConnectedClient.h"
#include "GameInformation.h"


#define FIELD_WIDTH 800
#define FIELD_HEIGHT 600
#define FIELD_MID_X FIELD_WIDTH/2
#define FIELD_MID_Y FIELD_HEIGHT/2

#define LEFT_BORDER_PLAYER 20
#define RIGHT_BORDER_PLAYER FIELD_WIDTH-20
#define TOP_BORDER_PLAYER 20
#define BOTTOM_BORDER_PLAYER FIELD_HEIGHT-20

#define LEFT_BORDER_BALL 10
#define RIGHT_BORDER_BALL FIELD_WIDTH-10
#define TOP_BORDER_BALL 10
#define BOTTOM_BORDER_BALL FIELD_HEIGHT-10

#define TOP_BORDER_GOAL 260
#define BOTTOM_BORDER_GOAL 339
#define TOP_BORDER_GOALKEEPER 270
#define BOTTOM_BORDER_GOALKEEPER 329

#define MAX_ONE_TEAM_PLAYERS 3
#define MAX_PLAYERS MAX_ONE_TEAM_PLAYERS*2
#define FIRST_TEAM 0
#define SECOND_TEAM 1

#define STEP 5
#define HALFSTEP 4
#define STEP_IF_CONTROL 3

#define BALL_KICK_VALUE 12
#define BALL_KICK_VALUE_DIAG 8

#define PLAYER_DIRECTION_LEFT 0
#define PLAYER_DIRECTION_RIGHT 1

#define GOALKEEPER_DIRECTION_UP -5
#define GOALKEEPER_DIRECTION_DOWN 5

#define GOALKEEPER_HOLDTIME 15

#define BALL_CONTROL_UNCATCHABLE -2
#define BALL_CONTROL_FREE -1
#define BALL_CONTROL_PLAYER(team, no) 2*no + team
#define BALL_CONTROL_GOALKEEPER(team) MAX_PLAYERS+team
#define BALL_CONTROL_FIRST_TEAM_GOALKEEPER MAX_PLAYERS+FIRST_TEAM
#define BALL_CONTROL_SECOND_TEAM_GOALKEEPER MAX_PLAYERS+SECOND_TEAM

#define BALL_POSITION_FIELD -2
#define BALL_POSITION_OUT -1
#define BALL_POSITION_FIRSTTEAMSCORED FIRST_TEAM
#define BALL_POSITION_SECONDTEAMSCORED SECOND_TEAM

#define BALL_DIRECTION_UNDEFINED 0
#define BALL_DIRECTION_LEFT 1
#define BALL_DIRECTION_RIGHT 2
#define BALL_DIRECTION_UP 3
#define BALL_DIRECTION_DOWN 4
#define BALL_DIRECTION_UPLEFT 5
#define BALL_DIRECTION_UPRIGHT 6
#define BALL_DIRECTION_DOWNLEFT 7
#define BALL_DIRECTION_DOWNRIGHT 8


class GameStatus
{
public:
	GameStatus();
	~GameStatus();

public:
	void initGame();
	void resetGame();

	void setPlayerEnabled(int team, int no, bool enabled);
	int getLastFreeNo(int team);
	int getTeamPlayerCount(int team);

	void movePlayerLeft(int team, int no);
	void movePlayerRight(int team, int no);
	void movePlayerUp(int team, int no);
	void movePlayerDown(int team, int no);
	void movePlayerUpLeft(int team, int no);
	void movePlayerUpRight(int team, int no);
	void movePlayerDownLeft(int team, int no);
	void movePlayerDownRight(int team, int no);

	void doPlayerKick(int team, int no);
	
	int sendStatus(ConnectedClient *client);

private:
	bool *teamEnabled;
	bool **playerEnabled;
	Position ***playerPosition;
	Position **goalkeeperPosition;
	Position *ballPosition;
	Score *score;
	int ballControl;	// -2 - ball is can't be caught; -1 - free; 0..MAX_ONE_TEAM_PLAYERS-1 - players; team+(2*MAX_ONE_TEAM_PLAYERS) - goalkeepers
	int goalkeeperHoldTime;
	int **playerDirection;	// PLAYER_DIRECTION_LEFT or PLAYER_DIRECTION_RIGHT
	int *goalkeeperDirection;	// GOALKEEPER_DIRECTION_UP or GOALKEEPER_DIRECTION_DOWN
	int ballDirection;	// BALL_DIRECTION_UNDEFINED..BALL_DIRECTION_DOWNRIGHT
	int rxBall;
	int ryBall;
	int playerStep;
	HANDLE moveGoalkeepersThreadHandle;
	bool deleting;

private:
	void placePlayers();

	inline void setTeamEnabled(int team, bool enabled);
	inline void setPlayerPosition(int team, int no, int x, int y);
	inline void setPlayerDirection(int team, int no, int direction);
	inline void setGoalkeeperPosition(int team, int x, int y);
	inline void setBallPosition(int x, int y);
	inline void setBallControl(int ballControl);
	inline void setScore(int firstNum, int secondNum);

	int checkBallPosition();	// player# or BALL_POSITION_FIELD or BALL_POSITION_OUT or BALL_POSITION_FIRSTTEAMSCORED or BALL_POSITION_SECONDTEAMSCORED
	inline void checkPlayerBallControl(int team, int no);
	inline void checkGoalkeeperBallControl(int team);

	inline void doScoreGoal(int team);
	void doGoalkeeperKick(int team);

	void moveGoalkeeper(int team);

	inline int getEnabledPlayerCount(int team);

	static DWORD WINAPI moveGoalkeepersThread(PVOID pParam);
	static DWORD WINAPI rollBallThread(PVOID pParam);

};

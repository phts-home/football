#include "StdAfx.h"
#include "GameInformation.h"

#pragma comment(lib, "Ws2_32.lib")


GameInformation::GameInformation(int firstScoreNum, int secondScoreNum, 
								 bool firstTeamEnabled, bool secondTeamEnabled, 
								 int firstGoalkeeperPositionX, int firstGoalkeeperPositionY, 
								 bool *firstTeamPlayerEnabled, int *firstTeamPlayerDirection, int *firstTeamPlayerPositionX, int *firstTeamPlayerPositionY, 
								 int secondGoalkeeperPositionX, int secondGoalkeeperPositionY, 
								 bool *secondTeamPlayerEnabled, int *secondTeamPlayerDirection, int *secondTeamPlayerPositionX, int *secondTeamPlayerPositionY, 
								 int ballPositionX, int ballPositionY)
{
	this->firstScoreNum = firstScoreNum;
	this->secondScoreNum = secondScoreNum;
	this->firstTeamEnabled = firstTeamEnabled;
	this->secondTeamEnabled = secondTeamEnabled;

	this->firstGoalkeeperPositionX = firstGoalkeeperPositionX;
	this->firstGoalkeeperPositionY = firstGoalkeeperPositionY;
	this->firstTeamPlayerEnabled = firstTeamPlayerEnabled;
	this->firstTeamPlayerDirection = firstTeamPlayerDirection;
	this->firstTeamPlayerPositionX = firstTeamPlayerPositionX;
	this->firstTeamPlayerPositionY = firstTeamPlayerPositionY;

	this->secondGoalkeeperPositionX = secondGoalkeeperPositionX;
	this->secondGoalkeeperPositionY = secondGoalkeeperPositionY;
	this->secondTeamPlayerEnabled = secondTeamPlayerEnabled;
	this->secondTeamPlayerDirection = secondTeamPlayerDirection;
	this->secondTeamPlayerPositionX = secondTeamPlayerPositionX;
	this->secondTeamPlayerPositionY = secondTeamPlayerPositionY;

	this->ballPositionX = ballPositionX;
	this->ballPositionY = ballPositionY;
}
GameInformation::~GameInformation()
{
	delete [] firstTeamPlayerEnabled;
	delete [] firstTeamPlayerDirection;
	delete [] firstTeamPlayerPositionX;
	delete [] firstTeamPlayerPositionY;
	delete [] secondTeamPlayerEnabled;
	delete [] secondTeamPlayerDirection;
	delete [] secondTeamPlayerPositionX;
	delete [] secondTeamPlayerPositionY;
}

int GameInformation::getFirstScoreNum()
{
	return firstScoreNum;
}

int GameInformation::getSecondScoreNum()
{
	return secondScoreNum;
}



bool GameInformation::isFirstTeamEnabled()
{
	return firstTeamEnabled;
}

bool GameInformation::isSecondTeamEnabled()
{
	return secondTeamEnabled;
}




int GameInformation::getFirstGoalkeeperPositionX()
{
	return firstGoalkeeperPositionX;
}

int GameInformation::getFirstGoalkeeperPositionY()
{
	return firstGoalkeeperPositionY;
}

bool GameInformation::isFirstTeamPlayerEnabled(int no)
{
	return firstTeamPlayerEnabled[no];
}

int GameInformation::getFirstTeamPlayerDirection(int no)
{
	return firstTeamPlayerDirection[no];
}

int GameInformation::getFirstTeamPlayerPositionX(int no)
{
	return firstTeamPlayerPositionX[no];
}

int GameInformation::getFirstTeamPlayerPositionY(int no)
{
	return firstTeamPlayerPositionY[no];
}



int GameInformation::getSecondGoalkeeperPositionX()
{
	return secondGoalkeeperPositionX;
}

int GameInformation::getSecondGoalkeeperPositionY()
{
	return secondGoalkeeperPositionY;
}

bool GameInformation::isSecondTeamPlayerEnabled(int no)
{
	return secondTeamPlayerEnabled[no];
}

int GameInformation::getSecondTeamPlayerDirection(int no)
{
	return secondTeamPlayerDirection[no];
}

int GameInformation::getSecondTeamPlayerPositionX(int no)
{
	return secondTeamPlayerPositionX[no];
}

int GameInformation::getSecondTeamPlayerPositionY(int no)
{
	return secondTeamPlayerPositionY[no];
}



int GameInformation::getBallPositionX()
{
	return ballPositionX;
}

int GameInformation::getBallPositionY()
{
	return ballPositionY;
}


GameInformation * GameInformation::receiveStatus(SOCKET s)
{
	int received;

	int _firstScoreNum;
	int _secondScoreNum;

	received = recv(s, (char *)&_firstScoreNum, sizeof(int), 0);
	received = recv(s, (char *)&_secondScoreNum, sizeof(int), 0);


	bool _firstTeamEnabled;
	received = recv(s, (char *)&_firstTeamEnabled, sizeof(bool), 0);

	int _firstGoalkeeperPositionX = 0;
	int _firstGoalkeeperPositionY = 0;

	bool *_firstTeamPlayerEnabled = (bool *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(bool));
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		_firstTeamPlayerEnabled[i] = false;
	}

	int *_firstTeamPlayerDirection = NULL;
	int *_firstTeamPlayerPositionsX = NULL;
	int *_firstTeamPlayerPositionsY = NULL;
	if (_firstTeamEnabled) {
		received = recv(s, (char *)&_firstGoalkeeperPositionX, sizeof(int), 0);
		received = recv(s, (char *)&_firstGoalkeeperPositionY, sizeof(int), 0);

		_firstTeamPlayerDirection = (int *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(int));
		_firstTeamPlayerPositionsX = (int *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(int));
		_firstTeamPlayerPositionsY = (int *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(int));
		for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
			_firstTeamPlayerDirection[i] = 0;
			_firstTeamPlayerPositionsX[i] = 0;
			_firstTeamPlayerPositionsY[i] = 0;
			received = recv(s, (char *)&_firstTeamPlayerEnabled[i], sizeof(bool), 0);
			if (_firstTeamPlayerEnabled[i]) {
				received = recv(s, (char *)&_firstTeamPlayerDirection[i], sizeof(int), 0);
				received = recv(s, (char *)&_firstTeamPlayerPositionsX[i], sizeof(int), 0);
				received = recv(s, (char *)&_firstTeamPlayerPositionsY[i], sizeof(int), 0);
			}
		}
	}


	bool _secondTeamEnabled;
	received = recv(s, (char *)&_secondTeamEnabled, sizeof(bool), 0);

	int _secondGoalkeeperPositionX = 0;
	int _secondGoalkeeperPositionY = 0;

	bool *_secondTeamPlayerEnabled = (bool *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(bool));
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		_secondTeamPlayerEnabled[i] = false;
	}

	int *_secondTeamPlayerDirection = NULL;
	int *_secondTeamPlayerPositionsX = NULL;
	int *_secondTeamPlayerPositionsY = NULL;
	if (_secondTeamEnabled) {
		received = recv(s, (char *)&_secondGoalkeeperPositionX, sizeof(int), 0);
		received = recv(s, (char *)&_secondGoalkeeperPositionY, sizeof(int), 0);
		
		_secondTeamPlayerDirection = (int *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(int));
		_secondTeamPlayerPositionsX = (int *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(int));
		_secondTeamPlayerPositionsY = (int *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(int));
		for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
			_secondTeamPlayerPositionsX[i] = 0;
			_secondTeamPlayerPositionsY[i] = 0;
			received = recv(s, (char *)&_secondTeamPlayerEnabled[i], sizeof(bool), 0);
			if (_secondTeamPlayerEnabled[i]) {
				received = recv(s, (char *)&_secondTeamPlayerDirection[i], sizeof(int), 0);
				received = recv(s, (char *)&_secondTeamPlayerPositionsX[i], sizeof(int), 0);
				received = recv(s, (char *)&_secondTeamPlayerPositionsY[i], sizeof(int), 0);
			}
		}
	}


	int _ballPositionX;
	int _ballPositionY;
	received = recv(s, (char *)&_ballPositionX, sizeof(int), 0);
	received = recv(s, (char *)&_ballPositionY, sizeof(int), 0);

	if (received <= SOCKET_ERROR) {
		return NULL;
	}

	return new GameInformation(_firstScoreNum, _secondScoreNum, _firstTeamEnabled, _secondTeamEnabled,
		_firstGoalkeeperPositionX, _firstGoalkeeperPositionY, _firstTeamPlayerEnabled, _firstTeamPlayerDirection, _firstTeamPlayerPositionsX, _firstTeamPlayerPositionsY,
		_secondGoalkeeperPositionX, _secondGoalkeeperPositionY, _secondTeamPlayerEnabled, _secondTeamPlayerDirection, _secondTeamPlayerPositionsX, _secondTeamPlayerPositionsY,
		_ballPositionX, _ballPositionY);
}


#pragma once

#include <Winsock2.h>
#include "ConnectedClient.h"
#include "GameStatus.h"
#include "ClientList.h"


#define PORT 4444

#define SERVERSOCKET_ALREADYCREATED 1
#define SERVERSOCKET_WSAFAILED 2
#define SERVERSOCKET_SOCKETFAILED 3
#define SERVERSOCKET_BINDFAILED 4
#define SERVERSOCKET_LISTENFAILED 5
#define SERVERSOCKET_OK 0

#define ACTION_LEFT 1
#define ACTION_RIGHT 2
#define ACTION_UP 3
#define ACTION_DOWN 4
#define ACTION_UPLEFT 5
#define ACTION_UPRIGHT 6
#define ACTION_DOWNLEFT 7
#define ACTION_DOWNRIGHT 8
#define ACTION_KICK 9

#define DATA_INIT 100
#define DATA_STATUS 101
#define DATA_SERVERFULL 102
#define DATA_REQUESTCLOSE 103
#define DATA_ACCEPTCLOSE 104
#define DATA_NEWGAME 105
#define DATA_GOAL 106

#define AUTO_TEAM 0
#define RED_TEAM 1
#define BLUE_TEAM 2


/*
 * Describes game server connection
 */
class GameServer
{
public:
	//
	// Creates server connection
	// Returns CONNECT_OK if connection is success
	//
	static int create();

	//
	// Closes server connection
	//
	static int close();

	//
	// Sends notifications to the clients about goal
	//
	static void notifyGoal();

private:
	static SOCKET serverSocket;
	static HANDLE observerThreadHandle;
	static HANDLE statusSenderThreadHandle;
	static ClientList *clientList;
	static int newClientId;
	static GameStatus *gameStatus;
	static bool created;

private:
	static void doClose();

	static void sendInitInformation(ConnectedClient *client);
	static void sendServerFullInformation(SOCKET clientSocket);

	static int sendData(SOCKET s, int data);

	static DWORD WINAPI observeClientsThread(PVOID pParam);
	static DWORD WINAPI receiveMessagesFromClientThread(PVOID pParam);
	static DWORD WINAPI statusSenderThread(PVOID pParam);
};


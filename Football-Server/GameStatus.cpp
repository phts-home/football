#include "StdAfx.h"
#include "GameStatus.h"

#include <winsock2.h>

#include "GameServer.h"

#pragma comment(lib, "Ws2_32.lib")


//////////////////////////////////////////////////////////////////////////
// Public constructors
//

GameStatus::GameStatus()
{
	score = new Score(0, 0);

	teamEnabled = (bool *)calloc(2, sizeof(bool));
	teamEnabled[0] = false;
	teamEnabled[1] = false;

	playerEnabled = (bool **)calloc(2, sizeof(bool *));
	playerEnabled[0] = (bool *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(bool));
	playerEnabled[1] = (bool *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(bool));

	playerPosition = (Position ***)calloc(2, sizeof(Position **));
	playerPosition[0] = (Position **)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(Position *));
	playerPosition[1] = (Position **)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(Position *));
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		playerPosition[0][i] = new Position(0, 0);
		playerPosition[1][i] = new Position(0, 0);
	}

	playerDirection = (int **)calloc(2, sizeof(int *));
	playerDirection[0] = (int *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(int));
	playerDirection[1] = (int *)calloc(MAX_ONE_TEAM_PLAYERS, sizeof(int));

	goalkeeperPosition = (Position **)calloc(2, sizeof(Position *));
	goalkeeperPosition[0] = new Position(0, 0);
	goalkeeperPosition[1] = new Position(0, 0);

	ballPosition = new Position(0, 0);

	goalkeeperDirection = (int*)calloc(2, sizeof(int));

	moveGoalkeepersThreadHandle = NULL;
	deleting = false;

	resetGame();
}

GameStatus::~GameStatus()
{
	resetGame();

	deleting = true;
	while (moveGoalkeepersThreadHandle != NULL) {
		Sleep(25);
	}

	delete score;

	delete [] playerEnabled[0];
	delete [] playerEnabled[1];
	delete [] playerEnabled;

	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		delete playerPosition[0][i];
		delete playerPosition[1][i];
	}
	delete [] playerPosition[0];
	delete [] playerPosition[1];
	delete [] playerPosition;

	delete [] playerDirection[0];
	delete [] playerDirection[1];
	delete [] playerDirection;

	delete goalkeeperPosition[0];
	delete goalkeeperPosition[1];
	delete [] goalkeeperPosition;

	delete [] teamEnabled;
	delete [] goalkeeperDirection;

	delete ballPosition;
}



//////////////////////////////////////////////////////////////////////////
// Public methods
//

void GameStatus::initGame()
{
	setScore(0, 0);
	placePlayers();
}

void GameStatus::resetGame()
{
	setScore(0, 0);

	teamEnabled[FIRST_TEAM] = false;
	teamEnabled[SECOND_TEAM] = false;

	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		playerEnabled[0][i] = false;
		playerEnabled[1][i] = false;
		playerDirection[0][i] = PLAYER_DIRECTION_RIGHT;
		playerDirection[1][i] = PLAYER_DIRECTION_LEFT;
	}

	ballControl = BALL_CONTROL_FREE;
	ballDirection = BALL_DIRECTION_UNDEFINED;

	goalkeeperDirection[0] = GOALKEEPER_DIRECTION_DOWN;
	goalkeeperDirection[1] = GOALKEEPER_DIRECTION_DOWN;

	goalkeeperHoldTime = 0;
	playerStep = STEP;
}

void GameStatus::setPlayerEnabled(int team, int no, bool enabled)
{
	if (enabled) {
		if (getEnabledPlayerCount(team) == 0) {
			setTeamEnabled(team, true);
		}
		playerEnabled[team][no] = enabled;
	} else {
		playerEnabled[team][no] = enabled;
		if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
			ballControl = BALL_CONTROL_FREE;
		}
		if (getEnabledPlayerCount(team) == 0) {
			setTeamEnabled(team, false);
		}
	}
}

int GameStatus::getLastFreeNo(int team)
{
	if ( (team < 0) || (team > 1) ) {
		return -1;
	}
	int n = 0;
	while ( (n < MAX_ONE_TEAM_PLAYERS) && (playerEnabled[team][n]) ) {
		n++;
	}
	return n;
}

int GameStatus::getTeamPlayerCount(int team)
{
	if ( (team < 0) || (team > 1) ) {
		return -1;
	}
	int n = 0;
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		if (playerEnabled[team][i]) {
			n++;
		}
	}
	return n;
}

void GameStatus::movePlayerLeft(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (!playerEnabled[team][no]) {
		return;
	}
	if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
		if (playerPosition[team][no]->getX() > LEFT_BORDER_PLAYER) {
			playerPosition[team][no]->move(-STEP_IF_CONTROL, 0);
		}
		ballPosition->setX(playerPosition[team][no]->getX() - 10);
		ballPosition->setY(playerPosition[team][no]->getY());
		ballDirection = BALL_DIRECTION_LEFT;
	} else {
		if (playerPosition[team][no]->getX() > LEFT_BORDER_PLAYER) {
			playerPosition[team][no]->move(-STEP, 0);
		}
		checkPlayerBallControl(team, no);
	}
	playerDirection[team][no] = PLAYER_DIRECTION_LEFT;
}

void GameStatus::movePlayerRight(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (!playerEnabled[team][no]) {
		return;
	}
	if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
		if (playerPosition[team][no]->getX() < RIGHT_BORDER_PLAYER) {
			playerPosition[team][no]->move(STEP_IF_CONTROL, 0);
		}
		ballPosition->setX(playerPosition[team][no]->getX() + 10);
		ballPosition->setY(playerPosition[team][no]->getY());
		ballDirection = BALL_DIRECTION_RIGHT;
	} else {
		if (playerPosition[team][no]->getX() < RIGHT_BORDER_PLAYER) {
			playerPosition[team][no]->move(STEP, 0);
		}
		checkPlayerBallControl(team, no);
	}
	playerDirection[team][no] = PLAYER_DIRECTION_RIGHT;
}

void GameStatus::movePlayerUp(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (!playerEnabled[team][no]) {
		return;
	}
	if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
		if (playerPosition[team][no]->getY() > TOP_BORDER_PLAYER) {
			playerPosition[team][no]->move(0, -STEP_IF_CONTROL);
		}
		ballPosition->setX(playerPosition[team][no]->getX());
		ballPosition->setY(playerPosition[team][no]->getY() - 10);
		ballDirection = BALL_DIRECTION_UP;
	} else {
		if (playerPosition[team][no]->getY() > TOP_BORDER_PLAYER) {
			playerPosition[team][no]->move(0, -STEP);
		}
		checkPlayerBallControl(team, no);
	}
}

void GameStatus::movePlayerDown(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (!playerEnabled[team][no]) {
		return;
	}
	if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
		if (playerPosition[team][no]->getY() < BOTTOM_BORDER_PLAYER) {
			playerPosition[team][no]->move(0, STEP_IF_CONTROL);
		}
		ballPosition->setX(playerPosition[team][no]->getX());
		ballPosition->setY(playerPosition[team][no]->getY() + 10);
		ballDirection = BALL_DIRECTION_DOWN;
	} else {
		if (playerPosition[team][no]->getY() < BOTTOM_BORDER_PLAYER) {
			playerPosition[team][no]->move(0, STEP);
		}
		checkPlayerBallControl(team, no);
	}
}

void GameStatus::movePlayerUpLeft(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (!playerEnabled[team][no]) {
		return;
	}
	if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
		if ( (playerPosition[team][no]->getX() > LEFT_BORDER_PLAYER) && (playerPosition[team][no]->getY() > TOP_BORDER_PLAYER) ) {
			playerPosition[team][no]->move(-STEP_IF_CONTROL, -STEP_IF_CONTROL);
		}
		ballPosition->setX(playerPosition[team][no]->getX() - 10);
		ballPosition->setY(playerPosition[team][no]->getY() - 10);
		ballDirection = BALL_DIRECTION_UPLEFT;
	} else {
		if ( (playerPosition[team][no]->getX() > LEFT_BORDER_PLAYER) && (playerPosition[team][no]->getY() > TOP_BORDER_PLAYER) ) {
			playerPosition[team][no]->move(-HALFSTEP, -HALFSTEP);
		}
		checkPlayerBallControl(team, no);
	}
	playerDirection[team][no] = PLAYER_DIRECTION_LEFT;
}

void GameStatus::movePlayerUpRight(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (!playerEnabled[team][no]) {
		return;
	}
	if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
		if ( (playerPosition[team][no]->getX() < RIGHT_BORDER_PLAYER) && (playerPosition[team][no]->getY() > TOP_BORDER_PLAYER) ) {
			playerPosition[team][no]->move(STEP_IF_CONTROL, -STEP_IF_CONTROL);
		}
		ballPosition->setX(playerPosition[team][no]->getX() + 10);
		ballPosition->setY(playerPosition[team][no]->getY() - 10);
		ballDirection = BALL_DIRECTION_UPRIGHT;
	} else {
		if ( (playerPosition[team][no]->getX() < RIGHT_BORDER_PLAYER) && (playerPosition[team][no]->getY() > TOP_BORDER_PLAYER) ) {
			playerPosition[team][no]->move(HALFSTEP, -HALFSTEP);
		}
		checkPlayerBallControl(team, no);
	}
	playerDirection[team][no] = PLAYER_DIRECTION_RIGHT;
}

void GameStatus::movePlayerDownLeft(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (!playerEnabled[team][no]) {
		return;
	}
	if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
		if ( (playerPosition[team][no]->getX() > LEFT_BORDER_PLAYER) && (playerPosition[team][no]->getY() < BOTTOM_BORDER_PLAYER) ) {
			playerPosition[team][no]->move(-STEP_IF_CONTROL, STEP_IF_CONTROL);
		}
		ballPosition->setX(playerPosition[team][no]->getX() - 10);
		ballPosition->setY(playerPosition[team][no]->getY() + 10);
		ballDirection = BALL_DIRECTION_DOWNLEFT;
	} else {
		if ( (playerPosition[team][no]->getX() > LEFT_BORDER_PLAYER) && (playerPosition[team][no]->getY() < BOTTOM_BORDER_PLAYER) ) {
			playerPosition[team][no]->move(-HALFSTEP, HALFSTEP);
		}
		checkPlayerBallControl(team, no);
	}
	playerDirection[team][no] = PLAYER_DIRECTION_LEFT;
}

void GameStatus::movePlayerDownRight(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (!playerEnabled[team][no]) {
		return;
	}
	if (ballControl == BALL_CONTROL_PLAYER(team, no)) {
		if ( (playerPosition[team][no]->getX() < RIGHT_BORDER_PLAYER) && (playerPosition[team][no]->getY() < BOTTOM_BORDER_PLAYER) ) {
			playerPosition[team][no]->move(STEP_IF_CONTROL, STEP_IF_CONTROL);
		}
		ballPosition->setX(playerPosition[team][no]->getX() + 10);
		ballPosition->setY(playerPosition[team][no]->getY() + 10);
		ballDirection = BALL_DIRECTION_DOWNRIGHT;
	} else {
		if ( (playerPosition[team][no]->getX() < RIGHT_BORDER_PLAYER) && (playerPosition[team][no]->getY() < BOTTOM_BORDER_PLAYER) ) {
			playerPosition[team][no]->move(HALFSTEP, HALFSTEP);
		}
		checkPlayerBallControl(team, no);
	}
	playerDirection[team][no] = PLAYER_DIRECTION_RIGHT;
}

void GameStatus::doPlayerKick(int team, int no)
{
	if ( (team < 0) || (team > 1) ) {
		return;
	}
	if ( (no < 0) || (no >= MAX_ONE_TEAM_PLAYERS) ) {
		return;
	}
	if (ballControl != BALL_CONTROL_FREE) {
		if (ballControl == BALL_CONTROL_PLAYER(team, no) ) {
			switch(ballDirection) {
				case BALL_DIRECTION_LEFT:
					{
						ballControl = BALL_CONTROL_UNCATCHABLE;
						rxBall = -BALL_KICK_VALUE;
						ryBall = 0;
						ballControl = BALL_CONTROL_FREE;
						CreateThread(NULL, 0, rollBallThread, this, 0, NULL);
						break;
					}
				case BALL_DIRECTION_RIGHT:
					{
						ballControl = BALL_CONTROL_UNCATCHABLE;
						rxBall = BALL_KICK_VALUE;
						ryBall = 0;
						ballControl = BALL_CONTROL_FREE;
						CreateThread(NULL, 0, rollBallThread, this, 0, NULL);
						break;
					}
				case BALL_DIRECTION_UP:
					{
						ballControl = BALL_CONTROL_UNCATCHABLE;
						rxBall = 0;
						ryBall = -BALL_KICK_VALUE;
						ballControl = BALL_CONTROL_FREE;
						CreateThread(NULL, 0, rollBallThread, this, 0, NULL);
						break;
					}
				case BALL_DIRECTION_DOWN:
					{
						ballControl = BALL_CONTROL_UNCATCHABLE;
						rxBall = 0;
						ryBall = BALL_KICK_VALUE;
						ballControl = BALL_CONTROL_FREE;
						CreateThread(NULL, 0, rollBallThread, this, 0, NULL);
						break;
					}
				case BALL_DIRECTION_UPLEFT:
					{
						ballControl = BALL_CONTROL_UNCATCHABLE;
						rxBall = -BALL_KICK_VALUE_DIAG;
						ryBall = -BALL_KICK_VALUE_DIAG;
						ballControl = BALL_CONTROL_FREE;
						CreateThread(NULL, 0, rollBallThread, this, 0, NULL);
						break;
					}
				case BALL_DIRECTION_UPRIGHT:
					{
						ballControl = BALL_CONTROL_UNCATCHABLE;
						rxBall = BALL_KICK_VALUE_DIAG;
						ryBall = -BALL_KICK_VALUE_DIAG;
						ballControl = BALL_CONTROL_FREE;
						CreateThread(NULL, 0, rollBallThread, this, 0, NULL);
						break;
					}
				case BALL_DIRECTION_DOWNLEFT:
					{
						ballControl = BALL_CONTROL_UNCATCHABLE;
						rxBall = -BALL_KICK_VALUE_DIAG;
						ryBall = BALL_KICK_VALUE_DIAG;
						ballControl = BALL_CONTROL_FREE;
						CreateThread(NULL, 0, rollBallThread, this, 0, NULL);
						break;
					}
				case BALL_DIRECTION_DOWNRIGHT:
					{
						ballControl = BALL_CONTROL_UNCATCHABLE;
						rxBall = BALL_KICK_VALUE_DIAG;
						ryBall = BALL_KICK_VALUE_DIAG;
						ballControl = BALL_CONTROL_FREE;
						CreateThread(NULL, 0, rollBallThread, this, 0, NULL);
						break;
					}
			}
		} else {
			if ( (abs(playerPosition[team][no]->getX() - ballPosition->getX()) <= 10) && (abs(playerPosition[team][no]->getY() - ballPosition->getY()) <= 10) ) {
				ballControl = BALL_CONTROL_PLAYER(team, no);
			}
		}
	}
}

int GameStatus::sendStatus(ConnectedClient *client)
{
	if (client == NULL) {
		return -1;
	}

	int _firstScoreNum = score->getNum(0);
	int _secondScoreNum = score->getNum(1);
	send(client->getSocket(), (char *)&_firstScoreNum, sizeof(int), 0);
	send(client->getSocket(), (char *)&_secondScoreNum, sizeof(int), 0);

	for (int i = 0; i < 2; i++) {
		send(client->getSocket(), (char *)&teamEnabled[i], sizeof(bool), 0);
		if (teamEnabled[i]) {
			int _goalkeeperPositionX = goalkeeperPosition[i]->getX();
			int _goalkeeperPositionY = goalkeeperPosition[i]->getY();
			send(client->getSocket(), (char *)&_goalkeeperPositionX, sizeof(int), 0);
			send(client->getSocket(), (char *)&_goalkeeperPositionY, sizeof(int), 0);
			for (int n = 0; n < MAX_ONE_TEAM_PLAYERS; n++) {
				send(client->getSocket(), (char *)&playerEnabled[i][n], sizeof(bool), 0);
				if (playerEnabled[i][n]) {
					int _playerDirection = playerDirection[i][n];
					int _playerPositionX = playerPosition[i][n]->getX();
					int _playerPositionY = playerPosition[i][n]->getY();
					send(client->getSocket(), (char *)&_playerDirection, sizeof(int), 0);
					send(client->getSocket(), (char *)&_playerPositionX, sizeof(int), 0);
					send(client->getSocket(), (char *)&_playerPositionY, sizeof(int), 0);
				}
			}
		}
	}
	
	int _ballPositionX = ballPosition->getX();
	int _ballPositionY = ballPosition->getY();
	send(client->getSocket(), (char *)&_ballPositionX, sizeof(int), 0);
	send(client->getSocket(), (char *)&_ballPositionY, sizeof(int), 0);

	return 0;
}




//////////////////////////////////////////////////////////////////////////
// Private methods
//

void GameStatus::placePlayers()
{
	goalkeeperHoldTime = 0;
	ballDirection = BALL_DIRECTION_UNDEFINED;
	setBallControl(BALL_CONTROL_FREE);
	setBallPosition(FIELD_MID_X, FIELD_MID_Y);
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		setPlayerPosition(0, i, FIELD_MID_X - 100 - i*50, FIELD_MID_Y);
		setPlayerPosition(1, i, FIELD_MID_X + 100 + i*50, FIELD_MID_Y);
		setPlayerDirection(0, i, PLAYER_DIRECTION_RIGHT);
		setPlayerDirection(1, i, PLAYER_DIRECTION_LEFT);
	}
	setGoalkeeperPosition(0, 20, FIELD_MID_Y);
	setGoalkeeperPosition(1, FIELD_WIDTH-20, FIELD_MID_Y);
}

void GameStatus::setTeamEnabled(int team, bool enabled)
{
	bool b1 = teamEnabled[FIRST_TEAM];
	bool b2 = teamEnabled[SECOND_TEAM];

	teamEnabled[team] = enabled;

	if (enabled && !b1 && !b2) {
		moveGoalkeepersThreadHandle = CreateThread(NULL, 0, moveGoalkeepersThread, this, 0, NULL);
	}
}

void GameStatus::setPlayerPosition(int team, int no, int x, int y)
{
	this->playerPosition[team][no]->setX(x);
	this->playerPosition[team][no]->setY(y);
}

void GameStatus::setPlayerDirection(int team, int no, int direction)
{
	playerDirection[team][no] = direction;
}

void GameStatus::setGoalkeeperPosition(int team, int x, int y)
{
	this->goalkeeperPosition[team]->setX(x);
	this->goalkeeperPosition[team]->setY(y);
}

void GameStatus::setBallPosition(int x, int y)
{
	this->ballPosition->setX(x);
	this->ballPosition->setY(y);
}

void GameStatus::setBallControl(int ballControl)
{
	this->ballControl = ballControl;
}

void GameStatus::setScore(int firstNum, int secondNum)
{
	score->setNum(0, firstNum);
	score->setNum(1, secondNum);
}

int GameStatus::checkBallPosition()
{
	if (ballPosition->getY() < TOP_BORDER_BALL) {
		ballPosition->setY(TOP_BORDER_BALL);
		return BALL_POSITION_OUT;
	}
	if (ballPosition->getY() > BOTTOM_BORDER_BALL) {
		ballPosition->setY(BOTTOM_BORDER_BALL);
		return BALL_POSITION_OUT;
	}
	if (ballPosition->getX() < LEFT_BORDER_BALL) {
		if ( (ballPosition->getY() > TOP_BORDER_GOAL) && (ballPosition->getY() < BOTTOM_BORDER_GOAL) ) {
			if ( ballPosition->getX() < 0 ) {
				doScoreGoal(SECOND_TEAM);
				return BALL_POSITION_SECONDTEAMSCORED;
			} else {
				return BALL_POSITION_FIELD;
			}
		}
		ballPosition->setX(LEFT_BORDER_BALL);
		return BALL_POSITION_OUT;
	}
	if (ballPosition->getX() > RIGHT_BORDER_BALL) {
		if ( (ballPosition->getY() > TOP_BORDER_GOAL) && (ballPosition->getY() < BOTTOM_BORDER_GOAL) ) {
			if ( ballPosition->getX() > FIELD_WIDTH ) {
				doScoreGoal(FIRST_TEAM);
				return BALL_POSITION_FIRSTTEAMSCORED;
			} else {
				return BALL_POSITION_FIELD;
			}
		}
		ballPosition->setX(RIGHT_BORDER_BALL);
		return BALL_POSITION_OUT;
	}
	return BALL_POSITION_FIELD;
}

void GameStatus::checkPlayerBallControl(int team, int no)
{
	if (ballControl == BALL_CONTROL_FREE) {
		if ( (abs(playerPosition[team][no]->getX() - ballPosition->getX()) <= 20) && (abs(playerPosition[team][no]->getY() - ballPosition->getY()) <= 20) ) {
			ballControl = BALL_CONTROL_PLAYER(team, no);
		}
	}
}

void GameStatus::checkGoalkeeperBallControl(int team)
{
	if (teamEnabled[team]) {
		if ( (abs(goalkeeperPosition[team]->getX() - ballPosition->getX()) <= 25) && (abs(goalkeeperPosition[team]->getY() - ballPosition->getY()) <= 25) ) {
			ballControl = BALL_CONTROL_GOALKEEPER(team);
			goalkeeperHoldTime = GOALKEEPER_HOLDTIME;
		}
	}
}


void GameStatus::doScoreGoal(int team)
{
	score->inc(team);
	placePlayers();

	GameServer::notifyGoal();
}

void GameStatus::doGoalkeeperKick(int team)
{
	if (ballControl == BALL_CONTROL_FIRST_TEAM_GOALKEEPER) {
		ballDirection = BALL_DIRECTION_UNDEFINED;
		ballControl = BALL_CONTROL_UNCATCHABLE;

 		rxBall = rand() % 15 + 15;
 		ryBall = rand() % 20 - 10;
		CreateThread(NULL, 0, rollBallThread, this, 0, NULL);

		return;
	}
	if (ballControl == BALL_CONTROL_SECOND_TEAM_GOALKEEPER) {
		ballDirection = BALL_DIRECTION_UNDEFINED;
		ballControl = BALL_CONTROL_UNCATCHABLE;

		rxBall = -(rand() % 15 + 15);
		ryBall = rand() % 20 - 10;
		CreateThread(NULL, 0, rollBallThread, this, 0, NULL);

		return;
	}
}

void GameStatus::moveGoalkeeper(int team)
{
	if (teamEnabled[team]) {
		if (ballControl == BALL_CONTROL_GOALKEEPER(team)) {
			ballPosition->setX(goalkeeperPosition[team]->getX());
			ballPosition->setY(goalkeeperPosition[team]->getY());
			goalkeeperHoldTime--;
			if (goalkeeperHoldTime <= 0) {
				doGoalkeeperKick(team);
			}
		} else {
			if ( (goalkeeperPosition[team]->getY() <= TOP_BORDER_GOALKEEPER) || (goalkeeperPosition[team]->getY() >= BOTTOM_BORDER_GOALKEEPER) ) {
				goalkeeperDirection[team] = -goalkeeperDirection[team];
			}
			goalkeeperPosition[team]->move(0, goalkeeperDirection[team]);
			checkGoalkeeperBallControl(team);
		}
	}
}

int GameStatus::getEnabledPlayerCount(int team)
{
	int c = 0;
	for (int i = 0; i < MAX_ONE_TEAM_PLAYERS; i++) {
		if (playerEnabled[team][i]) {
			c++;
		}
	}
	return c;
}

DWORD WINAPI GameStatus::moveGoalkeepersThread(PVOID pParam)
{
	GameStatus *stat = (GameStatus *)pParam;
	while (TRUE) {
		if ( (!stat->teamEnabled[FIRST_TEAM] && !stat->teamEnabled[SECOND_TEAM]) || (stat->deleting) ) {
			break;
		}
		for (int i = 0; i < 2; i++) {
			stat->moveGoalkeeper(i);
		}

		Sleep(100);
	}
	stat->moveGoalkeepersThreadHandle = NULL;
	return 0;
}

DWORD WINAPI GameStatus::rollBallThread(PVOID pParam)
{
	GameStatus *stat = (GameStatus *)pParam;
	for (int i = 0; i < 20; i++) {
		stat->ballPosition->move(2*stat->rxBall/(i/5+1), 2*stat->ryBall/(i/5+1));
		stat->checkGoalkeeperBallControl(FIRST_TEAM);
		stat->checkGoalkeeperBallControl(SECOND_TEAM);
		if (stat->ballControl >= 0) {
			break;
		}
		if (stat->checkBallPosition() != BALL_POSITION_FIELD) {
			break;
		}
		Sleep(25);
	}
	stat->ballControl = BALL_CONTROL_FREE;
	return 0;
}


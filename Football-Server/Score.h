#pragma once

class Score
{
public:
	Score(int firstNum, int secondNum);
	~Score();

public:
	int getNum(int pos); // pos == 0-1
	void setNum(int pos, int num);
	void inc(int pos);

private:
	int *nums;
};

#pragma once

#include "ConnectedClient.h"

class ClientList
{
public:
	ClientList(int maxSize);
	~ClientList();

public:
	int getSize();
	bool isFull();
	bool isEmpty();
	void add(ConnectedClient *client);
	ConnectedClient* get(int index);
	void deleteClient(ConnectedClient *client);
	void deleteAll();
	void removeClient(ConnectedClient *client);
	void removeAll();


private:
	ConnectedClient **clients;
	int size;
	int maxSize;

};

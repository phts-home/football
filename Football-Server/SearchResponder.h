#pragma once

#include <Winsock2.h>


#define PORT_UDP 4445

#define UDPSOCKET_ALREADYCREATED 1
#define UDPSOCKET_WSAFAILED 2
#define UDPSOCKET_SOCKETFAILED 3
#define UDPSOCKET_BINDFAILED 4
#define UDPSOCKET_OK 0

#define REQUEST_STRING "Football-Cl"
#define RESPONSE_STRING "Football-S"


/*
 * Class makes responds on client search requests using the UDP-connection
 */
class SearchResponder
{
public:
	//
	// Creates UDP-connection
	//
	static int create();

	//
	// Closes UDP-connection
	//
	static int close();

private:
	static SOCKET udpSocket;
	static HANDLE observeSearchRequestsThreadHandle;

private:
	static void doClose();

	static DWORD WINAPI observeSearchRequestsThread(PVOID pParam);

};

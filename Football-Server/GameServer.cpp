#include "StdAfx.h"
#include "GameServer.h"
#include "SearchResponder.h"

#pragma comment(lib, "Ws2_32.lib")




//////////////////////////////////////////////////////////////////////////
// Private parameters
//

SOCKET GameServer::serverSocket = NULL;

HANDLE GameServer::observerThreadHandle = NULL;
HANDLE GameServer::statusSenderThreadHandle = NULL;

ClientList *GameServer::clientList = NULL;

int GameServer::newClientId = 1;

GameStatus *GameServer::gameStatus = NULL;

bool GameServer::created = false;




//////////////////////////////////////////////////////////////////////////
// Public methods
//

int GameServer::create()
{
	printf("Create server connection... ");
	if (serverSocket != NULL) {
		printf("Connection has already been created\n");
		return SERVERSOCKET_ALREADYCREATED;
	}

	WSADATA wsd;
	if (WSAStartup(MAKEWORD(2,2), &wsd) != 0) {
		printf("WSAStartup() failed\n");
		return SERVERSOCKET_WSAFAILED;
	}

	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSocket == INVALID_SOCKET) {
		printf("socket() failed\n");
		WSACleanup();
		return SERVERSOCKET_SOCKETFAILED;
	}
	
	struct sockaddr_in local;
	local.sin_family = AF_INET;
	local.sin_port = htons(PORT);
	local.sin_addr.s_addr = htons(INADDR_ANY);

	int rc;
	rc = bind(serverSocket, (struct sockaddr *)&local, sizeof(local));
	if (rc == SOCKET_ERROR) {
		printf("bind() failed\n");
		WSACleanup();
		return SERVERSOCKET_BINDFAILED;
	}

	rc = listen(serverSocket, 5);
	if (rc == SOCKET_ERROR) {
		printf("listen() failed\n");
		WSACleanup();
		return SERVERSOCKET_LISTENFAILED;
	}

	printf("Done\n");

	clientList = new ClientList(MAX_ONE_TEAM_PLAYERS*2);
	gameStatus = new GameStatus();

	SearchResponder::create();

	created = true;

	observerThreadHandle = CreateThread(NULL, 0, observeClientsThread, NULL, 0, NULL);
	statusSenderThreadHandle = CreateThread(NULL, 0, statusSenderThread, NULL, 0, NULL);

	return SERVERSOCKET_OK;
}

int GameServer::close()
{
	printf("Close server connection... ");
	if (serverSocket == NULL) {
		printf("Connection has already been closed\n");
		return SERVERSOCKET_OK;
	}

	SearchResponder::close();

	closesocket(serverSocket);

	while (observerThreadHandle != NULL) {
		Sleep(25);
	}

	while (statusSenderThreadHandle != NULL) {
		Sleep(25);
	}

	while (!clientList->isEmpty()) {
		Sleep(25);
	}

	delete clientList;
	delete gameStatus;

	printf("Done\n");
	return SERVERSOCKET_OK;
}

void GameServer::notifyGoal()
{
	for (int i = 0; i < clientList->getSize(); i++) {
		sendData(clientList->get(i)->getSocket(), DATA_GOAL);
	}
}




//////////////////////////////////////////////////////////////////////////
// Private methods
//

void GameServer::doClose()
{
	serverSocket = NULL;
	observerThreadHandle = NULL;
	created = false;
	WSACleanup();
}

void GameServer::sendInitInformation(ConnectedClient *client)
{
	sendData(client->getSocket(), DATA_INIT);
	sendData(client->getSocket(), client->getTeam());
	sendData(client->getSocket(), client->getNo());
}

void GameServer::sendServerFullInformation(SOCKET clientSocket)
{
	sendData(clientSocket, DATA_SERVERFULL);
}

int GameServer::sendData(SOCKET s, int data)
{
	return send(s, (char *)&data, sizeof(int), 0);
}


DWORD WINAPI GameServer::observeClientsThread(PVOID pParam)
{
	while (TRUE) {
		printf("observeClients: wait\n");
		SOCKET connectedSocket = accept(serverSocket, NULL, NULL);
		if (connectedSocket == INVALID_SOCKET) {
			printf("accept() failed\n");
			for (int i = 0; i < clientList->getSize(); i++) {
				clientList->get(i)->close();
			}
			doClose();
			return 101;
		}
		printf("observeClients: accepted\n");

		if (clientList->isFull()) {
			sendServerFullInformation(connectedSocket);
			continue;
		}

		ConnectedClient *newClient = new ConnectedClient(connectedSocket, newClientId++);

		clientList->add(newClient);

		char buf[sizeof(int)];
		if (newClient->recvFromClient(buf, sizeof(buf)) > 0) {
			int* pact = (int*)buf;
			int act = *pact;
			printf("observeClients act == %d", act);
			if (act != DATA_INIT) {
				clientList->deleteClient(newClient);
				continue;
			}

			newClient->recvFromClient(buf, sizeof(buf));
			pact = (int*)buf;
			act = *pact;
			printf("observeClients act == %d", act);
			switch (act) {
				case AUTO_TEAM:
					{
						int s1 = gameStatus->getTeamPlayerCount(FIRST_TEAM);
						int s2 = gameStatus->getTeamPlayerCount(SECOND_TEAM);
						if ((s1 < MAX_ONE_TEAM_PLAYERS) && (s1 <= s2)) {
							newClient->setTeam(FIRST_TEAM);
						} else {
							if ((s2 < MAX_ONE_TEAM_PLAYERS) && (s2 <= s1)) {
								newClient->setTeam(SECOND_TEAM);
							}
						}
						newClient->setNo(gameStatus->getLastFreeNo(newClient->getTeam()));
						gameStatus->setPlayerEnabled(newClient->getTeam(), newClient->getNo(), true);
						sendInitInformation(newClient);
						CreateThread(NULL, 0, receiveMessagesFromClientThread, newClient, 0, NULL);
						break;
					}
				case RED_TEAM:
					{
						int s1 = gameStatus->getTeamPlayerCount(FIRST_TEAM);
						if (s1 < MAX_ONE_TEAM_PLAYERS) {
							newClient->setTeam(FIRST_TEAM);
						} else {
							int s2 = gameStatus->getTeamPlayerCount(SECOND_TEAM);
							if (s2 < MAX_ONE_TEAM_PLAYERS) {
								newClient->setTeam(SECOND_TEAM);
							}
						}
						newClient->setNo(gameStatus->getLastFreeNo(newClient->getTeam()));
						gameStatus->setPlayerEnabled(newClient->getTeam(), newClient->getNo(), true);
						sendInitInformation(newClient);
						CreateThread(NULL, 0, receiveMessagesFromClientThread, newClient, 0, NULL);
						break;
					}
				case BLUE_TEAM:
					{
						int s2 = gameStatus->getTeamPlayerCount(SECOND_TEAM);
						if (s2 < MAX_ONE_TEAM_PLAYERS) {
							newClient->setTeam(SECOND_TEAM);
						} else {
							int s1 = gameStatus->getTeamPlayerCount(FIRST_TEAM);
							if (s1 < MAX_ONE_TEAM_PLAYERS) {
								newClient->setTeam(FIRST_TEAM);
							}
						}
						newClient->setNo(gameStatus->getLastFreeNo(newClient->getTeam()));
						gameStatus->setPlayerEnabled(newClient->getTeam(), newClient->getNo(), true);
						sendInitInformation(newClient);
						CreateThread(NULL, 0, receiveMessagesFromClientThread, newClient, 0, NULL);
						break;
					}
				default:
					{
						clientList->deleteClient(newClient);
						continue;
					}
			}
		}

		// if new player is first
		if (clientList->getSize() == 1) {
			gameStatus->initGame();
		}
	}
	return 100;
}

DWORD WINAPI GameServer::receiveMessagesFromClientThread(PVOID pParam)
{
	ConnectedClient *client = (ConnectedClient *)pParam;
	int clientTeam;
	int clientNo;

	char buf[sizeof(int)];
	int received;
	while (TRUE) {
		received = client->recvFromClient(buf, sizeof(buf));
		clientTeam = client->getTeam();
		clientNo = client->getNo();
		if (received <= 0) {
			printf("SOCKET_ERROR\n");
			gameStatus->setPlayerEnabled(clientTeam, clientNo, false);
			clientList->deleteClient(client);
			return SOCKET_ERROR;
		}
		if (received != sizeof(buf)) {
			continue;
		}
		//printf("received == %d\n",received);
		int* pact = (int*)buf;
		int act = *pact;

		switch (act) {
			case ACTION_LEFT:
				{
					gameStatus->movePlayerLeft(clientTeam, clientNo);
					break;
				}
			case ACTION_RIGHT:
				{
					gameStatus->movePlayerRight(clientTeam, clientNo);
					break;
				}
			case ACTION_UP:
				{
					gameStatus->movePlayerUp(clientTeam, clientNo);
					break;
				}
			case ACTION_DOWN:
				{
					gameStatus->movePlayerDown(clientTeam, clientNo);
					break;
				}
			case ACTION_UPLEFT:
				{
					gameStatus->movePlayerUpLeft(clientTeam, clientNo);
					break;
				}
			case ACTION_UPRIGHT:
				{
					gameStatus->movePlayerUpRight(clientTeam, clientNo);
					break;
				}
			case ACTION_DOWNLEFT:
				{
					gameStatus->movePlayerDownLeft(clientTeam, clientNo);
					break;
				}
			case ACTION_DOWNRIGHT:
				{
					gameStatus->movePlayerDownRight(clientTeam, clientNo);
					break;
				}
			case ACTION_KICK:
				{
					gameStatus->doPlayerKick(clientTeam, clientNo);
					break;
				}
			case DATA_NEWGAME:
				{
					gameStatus->initGame();
					break;
				}
			case DATA_REQUESTCLOSE:
				{
					sendData(client->getSocket(), DATA_ACCEPTCLOSE);
					gameStatus->setPlayerEnabled(clientTeam, clientNo, false);
					clientList->deleteClient(client);
					return 201;
				}
		}
	}

	return 200;
}

DWORD WINAPI GameServer::statusSenderThread(PVOID pParam)
{
	while (TRUE) {
		if (!created) {
			break;
		}
		for (int i = 0; i < clientList->getSize(); i++) {
			sendData(clientList->get(i)->getSocket(), DATA_STATUS);
			gameStatus->sendStatus(clientList->get(i));
		}

		Sleep(25);
	}
	statusSenderThreadHandle = NULL;
	return 300;
}



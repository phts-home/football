#include "StdAfx.h"
#include "Score.h"

Score::Score(int firstNum, int secondNum)
{
	nums = (int *)calloc(2, sizeof(int));
	nums[0] = firstNum;
	nums[1] = secondNum;
}

Score::~Score()
{
	delete [] nums;
}

int Score::getNum(int pos)
{
	if ( (pos < 0) || (pos > 1) ) {
		return -1;
	}
	return nums[pos];
}

void Score::setNum(int pos, int num)
{
	if ( (pos < 0) || (pos > 1) ) {
		return;
	}
	nums[pos] = num;
}

void Score::inc(int pos)
{
	if ( (pos < 0) || (pos > 1) ) {
		return;
	}
	nums[pos] += 1;
}

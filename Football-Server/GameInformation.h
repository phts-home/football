#pragma once

#include <winsock2.h>

#include "GameStatus.h"

class GameInformation
{
public:
	GameInformation(int firstScoreNum, int secondScoreNum, 
		bool firstTeamEnabled, bool secondTeamEnabled,
		int firstGoalkeeperPositionX, int firstGoalkeeperPositionY,
		bool *firstTeamPlayerEnabled, int *firstTeamPlayerDirection, int *firstTeamPlayerPositionX, int *firstTeamPlayerPositionY,
		int secondGoalkeeperPositionX, int secondGoalkeeperPositionY,
		bool *secondTeamPlayerEnabled, int *secondTeamPlayerDirection, int *secondTeamPlayerPositionX, int *secondTeamPlayerPositionY,
		int ballPositionX, int ballPositionY);
	~GameInformation();

public:
	int getFirstScoreNum();
	int getSecondScoreNum();
	bool isFirstTeamEnabled();
	bool isSecondTeamEnabled();

	int getFirstGoalkeeperPositionX();
	int getFirstGoalkeeperPositionY();

	bool isFirstTeamPlayerEnabled(int no);
	int getFirstTeamPlayerDirection(int no);
	int getFirstTeamPlayerPositionX(int no);
	int getFirstTeamPlayerPositionY(int no);

	int getSecondGoalkeeperPositionX();
	int getSecondGoalkeeperPositionY();

	bool isSecondTeamPlayerEnabled(int no);
	int getSecondTeamPlayerDirection(int no);
	int getSecondTeamPlayerPositionX(int no);
	int getSecondTeamPlayerPositionY(int no);

	int getBallPositionX();
	int getBallPositionY();

	static GameInformation *receiveStatus(SOCKET s);

private:
	int firstScoreNum;
	int secondScoreNum;
	bool firstTeamEnabled;
	bool secondTeamEnabled;
	int firstGoalkeeperPositionX;
	int firstGoalkeeperPositionY;
	bool *firstTeamPlayerEnabled;
	int *firstTeamPlayerDirection;
	int *firstTeamPlayerPositionX;
	int *firstTeamPlayerPositionY;
	int secondGoalkeeperPositionX;
	int secondGoalkeeperPositionY;
	bool *secondTeamPlayerEnabled;
	int *secondTeamPlayerDirection;
	int *secondTeamPlayerPositionX;
	int *secondTeamPlayerPositionY;
	int ballPositionX;
	int ballPositionY;
};


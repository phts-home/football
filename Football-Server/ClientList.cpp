#include "StdAfx.h"
#include "ClientList.h"


ClientList::ClientList(int maxSize)
{
	clients = (ConnectedClient **)calloc(maxSize, sizeof(ConnectedClient *));
	this->maxSize = maxSize;
	size = 0;
}

ClientList::~ClientList()
{
	deleteAll();
	delete [] clients;
}

int ClientList::getSize()
{
	return size;
}

bool ClientList::isFull()
{
	return size >= maxSize;
}


bool ClientList::isEmpty()
{
	return size == 0;
}

void ClientList::add(ConnectedClient *client)
{
	if (size < maxSize) {
		clients[size] = client;
		size++;
	}
}

ConnectedClient* ClientList::get(int index)
{
	if (index >= size) {
		return NULL;
	}
	return clients[index];
}

void ClientList::deleteClient(ConnectedClient *client)
{
	if (client == NULL) {
		return;
	}
	int i = 0;
	while (i < size) {
		if (clients[i]->getId() == client->getId()) {
			delete clients[i];
			break;
		}
		i++;
	}

	// if no clients was removed
	if (i == size) {
		return;
	}
	while (i < size-1) {
		clients[i] = clients[i+1];
		i++;
	}
	size--;
}

void ClientList::deleteAll()
{
	for (int i = 0; i < size; i++) {
		delete clients[i];
	}
	size = 0;
}

void ClientList::removeClient(ConnectedClient *client)
{
	if (client == NULL) {
		return;
	}
	int i = 0;
	while (i < size) {
		if (clients[i]->getId() == client->getId()) {
			break;
		}
		i++;
	}

	// if no clients was removed
	if (i == size) {
		return;
	}
	while (i < size-1) {
		clients[i] = clients[i+1];
		i++;
	}
	size--;
}

void ClientList::removeAll()
{
	size = 0;
}

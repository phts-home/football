#pragma once

class Position
{
public:
	Position(int x, int y);

public:
	int getX();
	void setX(int x);
	int getY();
	void setY(int y);
	void move(int rx, int ry);

private:
	int x;
	int y;
};

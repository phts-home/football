#include "StdAfx.h"
#include "SearchResponder.h"

#pragma comment(lib, "Ws2_32.lib")




//////////////////////////////////////////////////////////////////////////
// Private parameters
//

SOCKET SearchResponder::udpSocket = NULL;
HANDLE SearchResponder::observeSearchRequestsThreadHandle = NULL;



//////////////////////////////////////////////////////////////////////////
// Public methods
//

int SearchResponder::create()
{
	printf("UDPServer: create\n");

	if (udpSocket != NULL) {
		printf("Connection has already been created\n");
		return UDPSOCKET_ALREADYCREATED;
	}

	WSADATA wsd;
	if (WSAStartup(MAKEWORD(2,2), &wsd) != 0) {
		printf("WSAStartup() failed\n");
		return UDPSOCKET_WSAFAILED;
	}

	udpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (udpSocket == INVALID_SOCKET) {
		printf("udp socket() failed\n");
		WSACleanup();
		return UDPSOCKET_SOCKETFAILED;
	}

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT_UDP);
	addr.sin_addr.s_addr = htons(INADDR_ANY);

	int r;
	r = bind(udpSocket, (struct sockaddr *)&addr, sizeof(addr));
	if (r == SOCKET_ERROR) {
		printf("udp bind() failed\n");
		WSACleanup();
		return UDPSOCKET_BINDFAILED;
	}

	observeSearchRequestsThreadHandle = CreateThread(NULL, 0, observeSearchRequestsThread, NULL, 0, NULL);

	return UDPSOCKET_OK;
}

int SearchResponder::close()
{
	closesocket(udpSocket);

	while (observeSearchRequestsThreadHandle != NULL) {
		Sleep(25);
	}

	return UDPSOCKET_OK;
}



//////////////////////////////////////////////////////////////////////////
// Private methods
//

void SearchResponder::doClose()
{
	udpSocket = NULL;
	observeSearchRequestsThreadHandle = NULL;
	WSACleanup();
}

DWORD WINAPI SearchResponder::observeSearchRequestsThread(PVOID pParam)
{
	const char* requestString = REQUEST_STRING;
	const char* responseString = RESPONSE_STRING;

	sockaddr_in clientAddr;
	int clientAddrSize = sizeof(clientAddr);

	int r;
	char buf[15];

	while (TRUE) {
		printf("observeSearchRequestsThread: while\n");
		r = recvfrom(udpSocket, buf, (int)strlen(requestString), 0, (sockaddr*)&clientAddr, &clientAddrSize);
		printf("observeSearchRequestsThread: recvfrom\n");
		if (r == SOCKET_ERROR) {
			printf("udp recvfrom() failed\n");
			doClose();
			return 151;
		}

		buf[strlen(requestString)] = 0;

		if (strcmp(buf, requestString) != 0) {
			continue;
		}

		r = sendto(udpSocket, responseString, (int)strlen(responseString), 0, (sockaddr*)&clientAddr, sizeof(clientAddr));
		if (r == SOCKET_ERROR) { 
			printf("udp sendto() failed\n");
			doClose();
			return 152; 
		}

	}

	return 150;
}

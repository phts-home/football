// Football-Server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "GameServer.h"



// current service status
SERVICE_STATUS ss;

// service status handle
SERVICE_STATUS_HANDLE ssHandle;

// lengthy operation steps counter
DWORD dwCheckPoint = 1;

// service name
LPTSTR FootballServiceName = "Football-Server";




/*
 * Sets the specified status for Football Service
 */
void SetFootballServiceStatus(DWORD dwCurrentState, DWORD dwWin32ExitCode, DWORD dwWaitHint)
{
	if(dwCurrentState == SERVICE_START_PENDING) {
		ss.dwControlsAccepted = 0;
	} else {
		ss.dwControlsAccepted = SERVICE_ACCEPT_STOP;
	}

	ss.dwCurrentState = dwCurrentState;
	ss.dwWin32ExitCode = dwWin32ExitCode;
	ss.dwWaitHint = dwWaitHint;

	if(dwCurrentState == SERVICE_RUNNING || dwCurrentState == SERVICE_STOPPED) {
		ss.dwCheckPoint = 0;
	} else {
		ss.dwCheckPoint = dwCheckPoint++;
	}

	SetServiceStatus(ssHandle, &ss);
}

/*
 * Stops Football Service
 */
BOOL StopFootballService()
{
	GameServer::close();
	return TRUE;
}

/*
 * Handles received control codes from SCM
 */
void WINAPI ServiceControl(DWORD dwControlCode)
{
	switch(dwControlCode) {
		case SERVICE_CONTROL_STOP: {
			SetFootballServiceStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);

			StopFootballService();

			SetFootballServiceStatus(SERVICE_STOPPED, NO_ERROR, 0);
			break;
		}
		case SERVICE_CONTROL_INTERROGATE: {
			SetFootballServiceStatus(ss.dwCurrentState, NO_ERROR, 0);
			break;
		}
		default: {
			SetFootballServiceStatus(ss.dwCurrentState, NO_ERROR, 0);
			break;
		}
	}
}

/*
 * Service entry point
 */
void WINAPI ServiceMain(DWORD dwArgc, LPSTR* lpszArgv)
{
	ssHandle = RegisterServiceCtrlHandler(FootballServiceName, ServiceControl);
	if(!ssHandle) {
		printf("Error registering ServiceControl\n");
		_getch();
		return;
	}
	ss.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
	SetFootballServiceStatus(SERVICE_START_PENDING, NO_ERROR, 4000);

	if (GameServer::create() == SERVERSOCKET_OK) {
		SetFootballServiceStatus(SERVICE_RUNNING, NO_ERROR, 0);
	} else {
		SetFootballServiceStatus(SERVICE_STOPPED, NO_ERROR, 0);
	};
}


/*
 * Application entry point
 */
int _tmain(int argc, _TCHAR* argv[])
{
	printf("Football-Server\n");
	printf("(c) Phil Tsarik, 2010\n\n");
	if (argc == 2) {
		if (strcmp(argv[1], "-console") == 0) {
			if (GameServer::create() == SERVERSOCKET_OK) {
				printf("Server connection successfully created\n");
				printf("\nPress any key to close connection ");
				_getch();
				GameServer::close();
				printf("\n\nServer connection closed\n");
			} else {
				printf("Failed to create connection\n");
			}
			
			printf("\nPress any key to exit ");
			_getch();
			return 0;
		}
	}

	printf("Usage: Football-Server.exe [-options...]\n\n");
	printf("where options include:\n\t-console - to run the application in console mode\n\n");
	printf("Or use this application as Windows service with Football-Server-Control.exe program\n");

	// entry points table
	SERVICE_TABLE_ENTRY DispatcherTable[] = {
		{
			FootballServiceName,                        // service name
			(LPSERVICE_MAIN_FUNCTION)ServiceMain        // main service function
		},
		{NULL, NULL}
	};

	// start dispatcher
	if(!StartServiceCtrlDispatcher(DispatcherTable)) {
		printf("\nPress any key to exit ");
		_getch();
		return -1;
   }
	return 0;
}


